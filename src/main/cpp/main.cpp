/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "opencv2/opencv.hpp"
#include "utils/file_utils.h"
#include "test/recognize_sign_test.h"

#define CVUI_IMPLEMENTATION

#include "ui/cvui.h"
#include "video/video_recorder.h"
#include "video/video_capture_manager.h"
#include "video/camera_tuner.h"
#include "ui/sign_view.h"
#include "processor/frame_processor.h"
#include "constants.h"
#include "utils/arc.h"

using namespace cv;
using namespace std;

int handleMode(string &root_dir, string &src, int mode, string &video_size);

void printHelp(const string &binary_name);

int main(const int argc, const char **argv) {
    cout << "EyeOnTheRoad started" << endl;
#ifdef ARCH_x86_64
    cout << "ARCH_x86_64" << endl;
#endif

#ifdef ARCH_aarch64
    cout << "ARCH_aarch64" << endl;
#endif
    cout << "OpenCV version: " << CV_VERSION << endl;

    string root_dir;
    string binary_name;
    string src = camera_id;
    int mode = default_mod;
    bool is_handle_mode = true;
    const Size size = SIZE_mid3_res;
    string video_size = to_string(size.width) + "x" + to_string(size.height);
//    cout << "Input args:" << endl;
    for (int k = 0; k < argc; ++k) {
        const string key = argv[k];
//        cout << "   arg:'" << key << "'" << endl;
        if (k == 0) {
            // Silly way to get root directory for this project:
            root_dir = key.substr(0, key.find_last_of('/'));
            binary_name = key.substr(key.find_last_of('/') + 1, key.size());
            root_dir = root_dir.substr(0, root_dir.find_last_of('/') + 1);
        }
        if (key == "-h" || key == "--help") {
            is_handle_mode = false;
            printHelp(binary_name);
        }

        string value;
        if (k + 1 < argc) {
            value = argv[k + 1];
        }
        if (key == "-m" || key == "--mode") {
            mode = static_cast<int>(strtol(value.c_str(), nullptr, 10));
        }
        if (key == "-src" || key == "--source") {
            src = value;
        }
        if (key == "-vs" || key == "--video_size") {
            video_size = value;
        }
    }

    cout << "Root dir:" << root_dir << endl;

    int result = 0;

    if (is_handle_mode) {
        result = handleMode(root_dir, src, mode, video_size);
    }

    cout << "EyeOnTheRoad ended" << endl;

    return result;
}

int processVideo(const string &output_dir, string &src, string &video_size,
                 SignView &sign_view, FrameProcessor &processor, DrawObjects &draw_objects) {
    vector<int> cameras = getNumOfCameras();
    cout << "Available cameras:" << endl;
    for (const int id : cameras) {
        cout << "- id:" << id << " name:" << (CAMERA_BASE_NAME + to_string(id)) << endl;
    }

    VideoCaptureManager video_capture_manager;
    video_capture_manager.openStream(src);
    video_capture_manager.handleInitSize(video_size);

    const int width = video_capture_manager.getWidth();
    const int height = video_capture_manager.getHeight();

    // Create window.
    namedWindow(frame_name, WND_PROP_FULLSCREEN);
    setWindowProperty(frame_name, WND_PROP_FULLSCREEN, WINDOW_FULLSCREEN);

    // Use this class to tune camera manually
    //CameraTuner camera_tuner(cap, frame_name);
    VideoRecorder video_recorder(output_dir, width, height);

    // Specify main video frame.
    Mat frame_origin, draw_frame;

    cvui::init(frame_name);

    bool do_quit;
    bool do_record = false;

    double zoom_factor_ = zoom_default;

    Size scaled_size;
    scaled_size.width = static_cast<int>(width / draw_frame_coef);
    scaled_size.height = static_cast<int>(height / draw_frame_coef);

    Rect dst_rc;
    auto zoomed_width = 0;
    auto zoomed_height = 0;

    // Set view for a Sign View to render detected sign on.
    Mat main_mat(height, width, CV_8UC3, COLOR_main_mat);
    Mat sign_view_frame(height, width, CV_8UC3, COLOR_main_mat);

    sign_view.setFrame(sign_view_frame);

    while (true) {
        auto start = getTimestampMilli();

        video_capture_manager.getFrame(frame_origin);
        if (frame_origin.empty()) {
            cout << "End of file detected" << endl;
            break;
        }

        do_quit = false;

        if (do_record) {
            Mat save_frame = frame_origin.clone();
            video_recorder.start(save_frame);
            save_frame.release();
        } else {
            video_recorder.stop();
        }

        if (zoom_factor_ > zoom_default) {
            zoomed_width = static_cast<int>(width / zoom_factor_);
            zoomed_height = static_cast<int>(height / zoom_factor_);
            resize(
                    frame_origin(
                            Rect((width - zoomed_width) >> 1,
                                 (height - zoomed_height) >> 1,
                                 zoomed_width,
                                 zoomed_height)
                    ),
                    frame_origin, scaled_size, 0, 0, INTER_LANCZOS4
            );
        }

        // Frame to use for the UI drawing.
        draw_frame = frame_origin.clone();

        processor.process(frame_origin);

        sign_view_frame.copyTo(main_mat);

        queue<DrawObject> rects = draw_objects.getObjects();
        while (!rects.empty()) {
            DrawObject draw_object = rects.front();
            rects.pop();
            rectangle(draw_frame, draw_object.rect, COLOR_red, 2);
        }

        resize(draw_frame, draw_frame, scaled_size, 0, 0, INTER_LANCZOS4);
        dst_rc.x = width - draw_frame.cols - video_frame_padding;
        dst_rc.y = video_frame_padding;
        dst_rc.width = draw_frame.cols;
        dst_rc.height = draw_frame.rows;
        draw_frame.copyTo(main_mat(dst_rc));

        // UI - Start
        if (cvui::button(main_mat, 20, 20, quit_label)) {
            do_quit = true;
        }
        if (cvui::button(main_mat, dst_rc.x, dst_rc.y + dst_rc.height + 20, minus_label)) {
            zoom_factor_ -= zoom_step;
            if (zoom_factor_ < zoom_default) {
                zoom_factor_ = zoom_default;
            }
        }
        cvui::text(main_mat, dst_rc.x + 50, dst_rc.y + dst_rc.height + 25, camera_zoom_label, 0.6);
        if (cvui::button(main_mat, dst_rc.x + 200, dst_rc.y + dst_rc.height + 20, plus_label)) {
            zoom_factor_ += zoom_step;
        }
        cvui::checkbox(main_mat, dst_rc.x + 380, dst_rc.y + dst_rc.height + 25, record_label, &do_record);
        cvui::update();
        // UI - End

        // Update sign view for each frame.
        sign_view.update();

        imshow(frame_name, main_mat);

        // Wait for the "q" button to quit.
        int code = waitKey(video_capture_manager.isOpened() ? 30 : 0) & 255;
        if (code == q_code || code == q_capital_code || code == esc_code || do_quit) {
            break;
        }

        cout << "Frame processed in " << (getTimestampMilli() - start) << " ms\n";
    }

    main_mat.release();
    sign_view_frame.release();
    draw_frame.release();
    frame_origin.release();

    video_recorder.stop();
    video_capture_manager.closeStream();
    destroyAllWindows();

    return 0;
}

int handleMode(string &root_dir, string &src, int mode, string &video_size) {
    // Specify directories and their values.
    static const string resources_dir(root_dir + "src/main/resources/");

    // Specify directories and their values.
    const string output_dir_name("output/");
    const string output_dir(root_dir + output_dir_name);
    createDirIfNeeded(root_dir, output_dir_name);

    cout << "Mode:" << mode << endl;
    cout << "Resources dir:" << resources_dir << endl;
    cout << "Output    dir:" << output_dir << endl;

    SignView sign_view(resources_dir);
    DrawObjects draw_objects;

    FrameProcessor processor(draw_objects, sign_view);
    processor.init(resources_dir, output_dir);
    processor.start();

    int result = 0;

    switch (mode) {
        case 0:
            processVideo(output_dir, src, video_size, sign_view, processor, draw_objects);
            break;
        case 1: {
            testProcessFrame(output_dir, src, processor, draw_objects);
            break;
        }
        default:
            cerr << "Unsupported mode:" << mode << endl;
            result = -1;
            break;
    }

    processor.stop();
    sign_view.stopRendering();

    return result;
}

void printHelp(const string &binary_name) {
    cout << "\nUsage: " << binary_name << " <option(s)>\n"
         << "\tOptions:\n"
         << "\t-h,  --help        Show help\n"
         << "\t-src,--source      Specify the source of stream\n"
         << "\t\tValues:\n"
         << "\t\t<N>                 - camera with id of N, by default the id 0 is selected\n"
         << "\t\t<url/to/video/file> - url to local video file\n"
         << "\t\t<url/to/test/file>  - url to test file in case of testing (if mode != 0)\n"
         << "\t-m,  --mode        Specify the mode to handle specified stream\n"
         << "\t\tValues:\n"
         << "\t\t0 - default value, means video processing mode\n"
         << "\t\t1 - testing (development only, process input single frame)\n"
         << "\t-vs, --video_size  Specify the size of the video frame. 320x240 or 960x540 "
            "or any other custom values separated by \"x\" sign.\n"
         << "\t                   Default value selected automatically depends on the camera and system capabilities\n"
         << endl;
}
