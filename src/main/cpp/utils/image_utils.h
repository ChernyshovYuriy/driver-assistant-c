/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_IMAGEUTILS_H
#define EYEONTHEROADCPP_IMAGEUTILS_H

#include <opencv2/opencv.hpp>
#include <cstdio>

using namespace std;
using namespace cv;

static const Size SIZE_ped_detect(320, 240);
static const Size SIZE_low_res(246, 135);
static const Size SIZE_mid3_res(640, 480);
static const Size SIZE_mid2_res(600, 337);
static const Size SIZE_mid_res(490, 270);
static const Size SIZE_high_res(960, 540);
static const Size SIZE_uhigh_res(1280, 720);
static const Size SIZE_xhigh_res(1920, 1080);
static const Size SIZE_zero(0, 0);
static const Size SIZE_one(1, 1);
static const Size SIZE_two(2, 2);
static const Size SIZE_three(3, 3);
static const Size SIZE_eight(8, 8);
static const Size SIZE_twenty(20, 20);
static const Size SIZE_thirty(30, 30);
static const Size SIZE_hundred(100, 100);
static const Size SIZE_two_hundred(200, 200);
// Width is 80, Height is 100.
static const Size SIZE_rect(80, 100);

inline static Mat resizeImageNoRatio(const Mat &img, const Size &size) {
    const int img_width = img.cols;
    if (img_width == 0) {
        throw invalid_argument("Size can not be of 0 width");
    }
    const int img_height = img.rows;
    if (img_height == 0) {
        throw invalid_argument("Size can not be of 0 height");
    }

    static Size size_resize_no_ratio;
    size_resize_no_ratio.width = size.width;
    size_resize_no_ratio.height = size.height;

    Mat img_resized;
//    cout << "Resize:" << img.cols << "x" << img.rows << " to:" << size.width << "x" << size.height << endl;
    resize(img, img_resized, size_resize_no_ratio, 0, 0, INTER_LANCZOS4);
    return img_resized;
}

inline static Mat resizeImage(const Mat &img, const Size &size) {
    const int img_width = img.cols;
    if (img_width == 0) {
        throw invalid_argument("Size can not be of 0 width");
    }
    const int img_height = img.rows;
    if (img_height == 0) {
        throw invalid_argument("Size can not be of 0 height");
    }
    double ratio;

    static Size size_resize;
    size_resize.width = size.width;
    size_resize.height = size.height;

    if (img_width > img_height) {
        ratio = size.width * 1.0 / img_width;
        size_resize.height = int(img_height * ratio);
    } else {
        ratio = size.height * 1.0 / img_height;
        size_resize.width = int(img_width * ratio);
    }
    Mat img_resized;
//    cout << "Resize:" << img.cols << "x" << img.rows << " to:" << size_resize.width << "x" << size_resize.height << endl;
    resize(img, img_resized, size_resize, 0, 0, INTER_LINEAR);
    return img_resized;
}

inline static Mat prepareImageToCompare(const Mat &image, const Size &size) {
    Mat imageScaled;
    resize(image, imageScaled, size);
    cvtColor(imageScaled, imageScaled, COLOR_BGR2GRAY);
    normalize(imageScaled, imageScaled, 0, 255, NORM_MINMAX);
    return imageScaled;
}

inline static void scaleRect(Rect &input, double xScale, double yScale) {
    input.x = static_cast<int>(input.x * xScale);
    input.y = static_cast<int>(input.y * yScale);
    input.width = static_cast<int>(input.width * xScale);
    input.height = static_cast<int>(input.height * yScale);
}

#endif //EYEONTHEROADCPP_IMAGEUTILS_H
