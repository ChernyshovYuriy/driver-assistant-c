/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_TIMEUTILS_H
#define EYEONTHEROADCPP_TIMEUTILS_H

#include <chrono>

using namespace std::chrono;

inline static long getTimestampNano() {
    return duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count();
}

inline static long getTimestampMilli() {
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

#endif //EYEONTHEROADCPP_TIMEUTILS_H
