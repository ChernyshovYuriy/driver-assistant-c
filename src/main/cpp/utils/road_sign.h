/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_ROADSIGN_H
#define EYEONTHEROADCPP_ROADSIGN_H

using namespace std;

static const int SIGNS_NUMBER = 30;

static const string RoadSignLabel[SIGNS_NUMBER] {
    "NO_LEFT_TURN",
    "NO_RIGHT_TURN",
    "NO_U_TURN",
    "STOP_SIGN",
    "NO_PARKING",
    "NO_STOPPING",
    "SPEED_LIMIT_5_NA",
    "SPEED_LIMIT_10_NA",
    "SPEED_LIMIT_15_NA",
    "SPEED_LIMIT_20_NA",
    "SPEED_LIMIT_25_NA",
    "SPEED_LIMIT_30_NA",
    "SPEED_LIMIT_35_NA",
    "SPEED_LIMIT_40_NA",
    "SPEED_LIMIT_45_NA",
    "SPEED_LIMIT_50_NA",
    "SPEED_LIMIT_55_NA",
    "SPEED_LIMIT_60_NA",
    "SPEED_LIMIT_65_NA",
    "SPEED_LIMIT_70_NA",
    "SPEED_LIMIT_75_NA",
    "SPEED_LIMIT_80_NA",
    "SPEED_LIMIT_85_NA",
    "SPEED_LIMIT_90_NA",
    "SPEED_LIMIT_95_NA",
    "SPEED_LIMIT_100_NA",
    "SPEED_LIMIT_110_NA",
    "SPEED_LIMIT_120_NA",
    "UNKNOWN",
};

enum RoadSign {
    NO_LEFT_TURN,
    NO_RIGHT_TURN,
    NO_U_TURN,
    STOP_SIGN,
    NO_PARKING,
    NO_STOPPING,
    NO_TRUCKS,
    SPEED_LIMIT_5_NA,
    SPEED_LIMIT_10_NA,
    SPEED_LIMIT_15_NA,
    SPEED_LIMIT_20_NA,
    SPEED_LIMIT_25_NA,
    SPEED_LIMIT_30_NA,
    SPEED_LIMIT_35_NA,
    SPEED_LIMIT_40_NA,
    SPEED_LIMIT_45_NA,
    SPEED_LIMIT_50_NA,
    SPEED_LIMIT_55_NA,
    SPEED_LIMIT_60_NA,
    SPEED_LIMIT_65_NA,
    SPEED_LIMIT_70_NA,
    SPEED_LIMIT_75_NA,
    SPEED_LIMIT_80_NA,
    SPEED_LIMIT_85_NA,
    SPEED_LIMIT_90_NA,
    SPEED_LIMIT_95_NA,
    SPEED_LIMIT_100_NA,
    SPEED_LIMIT_110_NA,
    SPEED_LIMIT_120_NA,
    UNKNOWN,
};

#endif //EYEONTHEROADCPP_ROADSIGN_H
