/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_FILEUTILS_H
#define EYEONTHEROADCPP_FILEUTILS_H

#include <iostream>
#include <sys/stat.h>
#include "opencv2/opencv.hpp"
#include "time_utils.h"

using namespace cv;
using namespace std;

static void createDirIfNeeded(const string &root_dir, const string &dir_name) {
    const string folder_name(root_dir + dir_name);
    const string folder_create_command("mkdir -p " + folder_name);
    const int result = system(folder_create_command.c_str());
    if (result == 0) {
        cout << "Folder '" << folder_name << "' created" << endl;
    } else {
        cout << "Folder '" << folder_name << "' not created, result:" << result << endl;
    }
}

static void saveMatToFile(const Mat &image_roi, const string &dir_path, const string &name_prefix) {
    const string file_name(dir_path + "/test_mat_" + name_prefix + to_string(getTimestampNano()) + ".png");
    imwrite(file_name, image_roi);
}

static void saveMatToFile(const Mat &image_roi, const string &dir_path) {
    saveMatToFile(image_roi, dir_path, "");
}

static int strEndWith(char *str, const char *suffix) {
    size_t strLen = strlen(str);
    size_t suffixLen = strlen(suffix);
    if (suffixLen <= strLen) {
        return strncmp(str + strLen - suffixLen, suffix, suffixLen) == 0;
    }
    return 0;
}

#endif //EYEONTHEROADCPP_FILEUTILS_H
