/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_COLORS_H
#define EYEONTHEROADCPP_COLORS_H

#include "opencv2/opencv.hpp"

using namespace cv;

static const Scalar COLOR_green(0, 255, 0);
static const Scalar COLOR_black(0, 0, 0);
static const Scalar COLOR_white(255, 255, 255);
static const Scalar COLOR_red(0, 0, 255);
static const Scalar COLOR_yellow(0, 255, 255);
static const Scalar COLOR_main_mat(139, 125, 96);

#endif //EYEONTHEROADCPP_COLORS_H
