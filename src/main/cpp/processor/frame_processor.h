/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_FRAME_PROCESSOR_H
#define EYEONTHEROADCPP_FRAME_PROCESSOR_H

#include <iostream>
#include "../ui/sign_view.h"
#include "../ui/draw_objects.h"

/**
 * This is obsolete class. Removed from use by new feature - DNN. Kept here to reference to speed limit value
 * recognition code.
 */

struct FrameProcessorInternalData;

class FrameProcessor {

public:
    explicit FrameProcessor(DrawObjects &draw_objects, SignView &sign_view);
    ~FrameProcessor();
    void init(const string &resources_dir, const string &output_dir);
    void process(const Mat& input_frame);
    void start();
    void stop();
    // This must be protected
    void run();

    enum  State {
        NOT_RUNNING,
        RUNNING
    };
private:
    State getState();
    unique_ptr<FrameProcessorInternalData> data_;
};

#endif //EYEONTHEROADCPP_FRAME_PROCESSOR_H
