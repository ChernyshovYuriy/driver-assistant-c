/*
 * Copyright 2019-2020 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <memory>
#include <thread>
#include <unordered_set>
#include <shared_mutex>

#include "opencv2/opencv.hpp"
#include "../utils/file_utils.h"
#include "../utils/road_sign.h"
#include "../utils/arc.h"
#include "../utils/string_utils.h"
#include "frame_processor.h"
#include "../dnn/default_dnn.h"

#ifdef ARCH_aarch64
#include "../yolo/yolo_dnn.h"
#endif

using namespace cv::ml;
using namespace cv;
using namespace std;

static constexpr const float PROBABILITY_THRESHOLD = 0.7;
static constexpr const uint8_t SPEED_LIMIT_CHARS_SIZE = 12;
// Must match name from the obj.names file
static constexpr const char *const SPEED_LIMIT = "SPEED_LIMIT_";
static constexpr const char *const STOP = "STOP";

struct FrameProcessorInternalData {

    explicit FrameProcessorInternalData(DrawObjects &draw_objects,
                                        SignView &sign_view) : draw_objects(draw_objects), sign_view(sign_view) {

    }

    volatile FrameProcessor::State state = FrameProcessor::State::NOT_RUNNING;
    FrameProcessor *weak_reference{};
    pthread_t thread_id{};
    queue<Mat> queued_frames;
    mutex queue_mutex;
    mutex state_mutex;
    struct timespec state_time_spec{};

    unique_ptr<DNN> dnn = nullptr;
    DrawObjects &draw_objects;
    SignView &sign_view;
    string output_dir;

    void setState(FrameProcessor::State value) {
        lock_guard<mutex> write_lock(state_mutex);
        this->state = value;
    }

    FrameProcessor::State getState() {
        lock_guard<mutex> read_lock(state_mutex);
        return state;
    }

    bool waitForState(FrameProcessor::State value, int timeout) {
        FrameProcessor::State current_state = getState();
        if (current_state == value) {
            return true;
        }
        state_time_spec.tv_sec = time(nullptr) + timeout;
        state_time_spec.tv_nsec = 0;
        while (getState() != value) {
            // TODO: Improve with number of attempts
            this_thread::sleep_for(chrono::milliseconds{100});
        }
        current_state = getState();
        return current_state == value;
    }

    void run() {
        setState(FrameProcessor::State::RUNNING);
        weak_reference->run();
    }
};

static void *handle_frame_thread(void *thread_args) {
    auto *processor = (FrameProcessorInternalData *) thread_args;
    processor->run();
}

FrameProcessor::FrameProcessor(DrawObjects &draw_objects, SignView &sign_view) {
    data_ = make_unique<FrameProcessorInternalData>(draw_objects, sign_view);
    data_->weak_reference = this;
#ifdef ARCH_x86_64
    data_->dnn = make_unique<DefaultDNN>();
#endif

#ifdef ARCH_aarch64
    data_->dnn = make_unique<YoloDNN>();
#endif
}

FrameProcessor::~FrameProcessor() = default;

void FrameProcessor::init(const string &resources_dir, const string &output_dir) {
    data_->dnn->init(resources_dir, output_dir);
    data_->output_dir = output_dir;
}

void FrameProcessor::start() {
    if (getState() != NOT_RUNNING) {
        cerr << "Frame thread already running" << endl;
        return;
    }
    int result = pthread_create(&data_->thread_id, nullptr, handle_frame_thread, data_.get());
    cout << "Frame thread create result " << result << endl;
    if (result != 0 || !(data_->waitForState(RUNNING, 5))) {
        cerr << "Failed to wait for Frame thread to start" << endl;
        return;
    }
}

void FrameProcessor::stop() {
    if (getState() != RUNNING) {
        cerr << "Frame thread already stopped" << endl;
        return;
    }
    data_->setState(NOT_RUNNING);
    if (!(data_->waitForState(NOT_RUNNING, 5))) {
        cerr << "Failed to wait for Frame thread to stop" << endl;
        return;
    }
    int result = pthread_join(data_->thread_id, nullptr);
    cout << "Frame thread join result " << result << endl;
}

void FrameProcessor::process(const Mat &input_frame) {
    lock_guard<mutex> writer_lock(data_->queue_mutex);
    data_->queued_frames.push(input_frame.clone());
}

void FrameProcessor::run() {
    cout << "Frame thread run begin" << endl;
    static const int drop_number = 0;
    int counter = 0;
    while (getState() == RUNNING) {
        while (!data_->queued_frames.empty()) {
            auto start = getTimestampMilli();
            Mat input_frame = data_->queued_frames.front();
            data_->queued_frames.pop();

            if (counter++ < drop_number) {
                continue;
            }
            counter = 0;

            // Do logic here
            vector<dnn_return_data_t> dnn_result = data_->dnn->onFrame(input_frame);
            for (const dnn_return_data_t &dnn_data : dnn_result) {
                cout << dnn_data.name << ", confidence:" << dnn_data.probability << endl;

                if (dnn_data.probability <= PROBABILITY_THRESHOLD) {
//                    continue;
                }

                Rect rect(dnn_data.box);

                if (rect.x < 0) rect.x = 0;
                if (rect.x > input_frame.cols) rect.x = input_frame.cols;
                if (rect.y < 0) rect.y = 0;
                if (rect.y > input_frame.rows) rect.y = input_frame.rows;
                if (rect.x + rect.width > input_frame.cols) rect.width = input_frame.cols - rect.x;
                if (rect.y + rect.height > input_frame.rows) rect.height = input_frame.rows - rect.y;

                if (rect.width == 0 || rect.height == 0) {
                    continue;
                }

                const DrawObject draw_object(rect);
                data_->draw_objects.addObject(draw_object);

//                Mat roi = input_frame(rect);
//                cvtColor(roi, roi, COLOR_RGB2GRAY);
//                saveMatToFile(roi, data_->output_dir, "test");
                if (startsWith(dnn_data.name, SPEED_LIMIT)) {
                    string valueStr = dnn_data.name.substr(SPEED_LIMIT_CHARS_SIZE, dnn_data.name.length());
                    string::size_type sz;
                    data_->sign_view.showSign(getSpeedLimitSignCanada(stoi(valueStr, &sz)));
                }
                if (startsWith(dnn_data.name, STOP)) {
                    data_->sign_view.showSign(STOP_SIGN);
                }
            }

            cout << "  dnn processed in " << (getTimestampMilli() - start) << " ms, size:"
                 << data_->queued_frames.size() << "\n";
            if (getState() != RUNNING) {
                break;
            }
        }
    }
    cout << "Frame thread run completed" << endl;
}

FrameProcessor::State FrameProcessor::getState() {
    return data_->getState();
}
