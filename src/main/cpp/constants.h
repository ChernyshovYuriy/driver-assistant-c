/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_CONSTANTS_H
#define EYEONTHEROADCPP_CONSTANTS_H

#include <string>
#include "utils/image_utils.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Camera
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static constexpr const char *const default_camera_id = "0";
static constexpr const char *const first_camera_id = "1";
static constexpr const char *const camera_id = default_camera_id;
static constexpr const int default_mod = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GUI
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static constexpr const int esc_code = 27;
static constexpr const int q_code = 'q';
static constexpr const int q_capital_code = 'Q';
static constexpr const char *const minus_label = "-";
static constexpr const char *const plus_label = "+";
static constexpr const char *const x_label = "x";
static constexpr const char *const quit_label = "Quit";
static constexpr const char *const record_label = "Record";
static constexpr const char *const camera_zoom_label = "Camera Zoom";
static constexpr const char *const frame_name = "Eye On The road";

static const float draw_frame_coef = (SIZE_uhigh_res.width * 1.0F) / SIZE_high_res.width;
static const int video_frame_padding = 5;
static const double zoom_step = 0.1;
static const double zoom_default = 1.0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// YOLO
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static constexpr const char *const model_name = "dnn/yolo/v4/yolov4-tiny.weights";
static constexpr const char *const config_name = "dnn/yolo/v4/yolov4-tiny.cfg";
static constexpr const char *const obj_names = "dnn/yolo/v4/obj.names";
static constexpr const char *const NET_NAME = "Darknet";
static const float blob_scale = 1.0;
static const Scalar blob_scalar = Scalar();
static const Scalar net_mean = Scalar(0.0, 0.0, 0.0);
// Create a 4D blob from a frame.
static Size inp_size(416, 416);
static const float net_scale = 0.0392;
static constexpr const char *const net_input_name = "";

#endif //EYEONTHEROADCPP_CONSTANTS_H
