/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_DIGITSOCR_H
#define EYEONTHEROADCPP_DIGITSOCR_H

#include <string>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;
using namespace cv::ml;

class DigitsOCR {

public:
    DigitsOCR ();
    ~DigitsOCR ();

    void init(string &resources_dir, string &output_dir_path);
    int recognize(Mat &number_mat, int type);

private:
    Ptr<SVM> svm;
    int descriptor_size;
    HOGDescriptor hog;
    string output_dir_path;
    Mat response;
};

#endif //EYEONTHEROADCPP_DIGITSOCR_H
