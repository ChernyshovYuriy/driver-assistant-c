/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "digits_ocr.h"
#include "../utils/image_utils.h"
#include "../utils/file_utils.h"
#include <dirent.h>

static const int digit_size = 20;
static const Size digit_size_obj(digit_size, digit_size);
static float affine_flags = WARP_INVERSE_MAP | INTER_LINEAR;

DigitsOCR::DigitsOCR() = default;

DigitsOCR::~DigitsOCR() = default;

void DigitsOCR::init(string &resources_dir, string &output_dir_path) {
    this->output_dir_path = output_dir_path;

    hog.winSize = Size(20, 20);
    hog.blockSize = Size(10, 10);
    hog.blockStride = Size(5, 5);
    hog.cellSize = Size(10, 10);
    hog.nbins = 9;
    hog.derivAperture = 1;
    hog.winSigma = -1;
    hog.histogramNormType = HOGDescriptor::HistogramNormType::L2Hys;
    hog.L2HysThreshold = 0.2;
    hog.gammaCorrection = false;
    hog.nlevels = 64;
    hog.signedGradient = true;

    vector<int> train_labels;
    vector<Mat> train_cells;
    Mat img = imread(resources_dir + "/digits/digits.png", IMREAD_GRAYSCALE);
    for (int i = 0; i < img.rows; i = i + digit_size) {
        for (int j = 0; j < img.cols; j = j + digit_size) {
            Mat digit_img = (img.colRange(j, j + digit_size).rowRange(i, i + digit_size)).clone();
            train_cells.push_back(digit_img);
        }
    }
    int digit_class_number = 0;
    for (int z = 0; z < train_cells.size(); ++z) {
        if (z % 450 == 0 && z != 0) {
            digit_class_number++;
        }
        train_labels.push_back(digit_class_number);
    }

    DIR *images_dir;
    DIR *image_dir;
    struct dirent *ent_dir;
    struct dirent *ent_file;
    string images_dir_path(resources_dir + "/digits/custom");
    string image_dir_path;
    int count = 0;
    cout << "Custom digits training ..." << endl;
    if ((images_dir = opendir(images_dir_path.c_str())) != nullptr) {
        while ((ent_dir = readdir(images_dir)) != nullptr) {
            if (ent_dir->d_type != DT_DIR) {
                continue;
            }
            image_dir_path = images_dir_path + "/" + ent_dir->d_name;
            if ((image_dir = opendir(image_dir_path.c_str())) != nullptr) {
                while ((ent_file = readdir(image_dir)) != nullptr) {
                    if (strEndWith(ent_file->d_name, ".jpg") == 0) {
                        continue;
                    }
                    string img_path(image_dir_path + "/" + ent_file->d_name);
                    Mat src = imread(img_path, IMREAD_GRAYSCALE);
                    int label = stoi(ent_dir->d_name);

                    train_cells.push_back(src);
                    train_labels.push_back(label);
                    count++;
                }
            }
            closedir(image_dir);
        }
        closedir(images_dir);
    }
    cout << "Custom digits training completed, trained on " << count << " custom images" << endl;

    vector<vector<float> > train_hog;
    for (const auto &train_cell : train_cells) {
        vector<float> descriptors;
        hog.compute(train_cell, descriptors);
        train_hog.push_back(descriptors);
    }

    descriptor_size = static_cast<int>(train_hog[0].size());
    cout << "Descriptor Size : " << descriptor_size << endl;

    Mat train_mat(static_cast<int>(train_hog.size()), descriptor_size, CV_32FC1);
    for (int i = 0; i < train_hog.size(); i++) {
        for (int j = 0; j < descriptor_size; j++) {
            train_mat.at<float>(i, j) = train_hog[i][j];
        }
    }

    svm = SVM::create();
    svm->setGamma(0.50625);
    svm->setC(12.5);
    svm->setKernel(SVM::RBF);
    svm->setType(SVM::C_SVC);
    Ptr<TrainData> td = TrainData::create(train_mat, ROW_SAMPLE, train_labels);
    svm->train(td);
    svm->save(resources_dir + "/digits/model4.yml");

    img.release();
    train_mat.release();
    train_hog.clear();
    train_labels.clear();
    train_cells.clear();
}

int DigitsOCR::recognize(Mat &number_mat, int type) {
    Mat black(20, 20, type, Scalar(0));

    number_mat = resizeImage(number_mat, digit_size_obj);
    number_mat.copyTo(black(Rect((20 - number_mat.cols) >> 1, 0, number_mat.cols, number_mat.rows)));

//    saveMatToFile(black, output_dir);

    vector<float> descriptors;
    hog.compute(black, descriptors);

    Mat response_mat(static_cast<int>(1), descriptor_size, CV_32FC1);
    for (int j = 0; j < descriptor_size; ++j) {
        response_mat.at<float>(0, j) = descriptors[j];
    }

    Mat response;
    svm->predict(response_mat, response);

    const auto value = static_cast<int>(response.at<float>(0, 0));

//    descriptors.clear();
//    black.release();
//    response_mat.release();

    return value;
}
