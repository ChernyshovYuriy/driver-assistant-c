/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "yolo_dnn.h"

#include <memory>

#include "trt_utils.h"
#include "yolo.h"
#include "yolo_config_parser.h"
#include "yolov3.h"

#include <experimental/filesystem>
#include <string>

using namespace cv;

struct DNNInternalData {

    unique_ptr<Yolo> infer_net;
    uint batch_size = 1;
    int inputH = 0;
    int inputW = 0;
    int resizeH = 0;
    int resizeW = 0;
    int xOffset = 0;
    int yOffset = 0;
};

YoloDNN::YoloDNN() {
    data_ = make_unique<DNNInternalData>();
}

YoloDNN::~YoloDNN() = default;

void YoloDNN::init(const string &resources_dir, const string &output_dir) {
//    Batch size:1
//    Yolo NetworkInfo:
//        networkType:yolov3-tiny
//        configFilePath:data/yolov3-tiny.cfg
//        wtsFilePath:data/yolov3-tiny.weights
//        labelsFilePath:data/labels.txt
//        precision:kFLOAT
//        deviceType:kGPU
//        calibrationTablePath:data/yolov3-tiny-calibration.table
//        enginePath:data/yolov3-tiny-kFLOAT-kGPU-batch1.engine
//        inputBlobName:data
//    Yolo InferParams:
//        printPerfInfo:0
//        printPredictionInfo:0
//        calibImages:data/calibration_images.txt
//        calibImagesPath:
//        probThresh:0.5
//        nmsThresh:0.5

    string arg2str = "--config_file_path=" + (resources_dir + config_name);
    string arg3str = "--wts_file_path=" + (resources_dir + model_name);
    string arg4str = "--labels_file_path=" + (resources_dir + obj_names);

    char *arg2 = new char[arg2str.length() + 1];
    char *arg3 = new char[arg3str.length() + 1];
    char *arg4 = new char[arg4str.length() + 1];

    strcpy(arg2, arg2str.c_str());
    strcpy(arg3, arg3str.c_str());
    strcpy(arg4, arg4str.c_str());

    char *argv[] = {
            "dummy_string_arg",
            "--network_type=yolov3-tiny",
            arg2, arg3, arg4
    };
    int argc = 5;

    yoloConfigParserInit(argc, argv);
    NetworkInfo yolo_info = getYoloNetworkInfo();
    InferParams yolo_infer_params = getYoloInferParams();

    yolo_infer_params.nmsThresh = 0.4;

    cout << "Batch size:" << data_->batch_size << endl;
    cout << "Yolo NetworkInfo: {"
         << "networkType:" << yolo_info.networkType << " "
         << "configFilePath:" << yolo_info.configFilePath << " "
         << "wtsFilePath:" << yolo_info.wtsFilePath << " "
         << "labelsFilePath:" << yolo_info.labelsFilePath << " "
         << "precision:" << yolo_info.precision << " "
         << "deviceType:" << yolo_info.deviceType << " "
         << "calibrationTablePath:" << yolo_info.calibrationTablePath << " "
         << "enginePath:" << yolo_info.enginePath << " "
         << "inputBlobName:" << yolo_info.inputBlobName
         << endl;
    cout << "Yolo InferParams: {"
         << "printPerfInfo:" << yolo_infer_params.printPerfInfo << " "
         << "printPredictionInfo:" << yolo_infer_params.printPredictionInfo << " "
         << "calibImages:" << yolo_infer_params.calibImages << " "
         << "calibImagesPath:" << yolo_infer_params.calibImagesPath << " "
         << "probThresh:" << yolo_infer_params.probThresh << " "
         << "nmsThresh:" << yolo_infer_params.nmsThresh
         << endl;

    data_->infer_net = unique_ptr<Yolo>{new YoloV3(data_->batch_size, yolo_info, yolo_infer_params)};

    delete[] arg2;
    delete[] arg3;
    delete[] arg4;
}

vector<dnn_return_data_t> YoloDNN::onFrame(Mat &frame) {

    Mat img = scale_to_blob(frame);
    Mat trt_input = blobFromImage(img, 1.0, inp_size);
    data_->infer_net->doInference(trt_input.data, 1);

    auto binfo = data_->infer_net->decodeDetections(0, frame.rows, frame.cols);
    auto remaining = nmsAllClasses(
            data_->infer_net->getNMSThresh(), binfo, data_->infer_net->getNumClasses()
    );

    vector<dnn_return_data_t> result;
    for (auto box : remaining) {
        int x = box.box.x1;
        int y = box.box.y1;
        int w = box.box.x2 - box.box.x1;
        int h = box.box.y2 - box.box.y1;
        if (x < 0) x = 0;
        if (x > frame.cols) x = frame.cols;
        if (y < 0) y = 0;
        if (y > frame.rows) y = frame.rows;
        if (x + w > frame.cols) w = frame.cols - x;
        if (y + h > frame.rows) h = frame.rows - y;
        Rect rect(x, y, w, h);
        result.push_back({data_->infer_net->getClassName(box.label), box.prob, rect});
    }

    return result;
}
