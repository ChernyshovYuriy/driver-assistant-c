/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_YOLO_DNN_H
#define EYEONTHEROADCPP_YOLO_DNN_H

#include "../dnn/dnn.h"

using namespace std;

struct DNNInternalData;

class YoloDNN : public DNN {

public:
    YoloDNN();

    ~YoloDNN();

    void init(const string &resources_dir, const string &output_dir) override ;

    vector<dnn_return_data_t> onFrame(Mat &frame) override ;

private:
    unique_ptr<DNNInternalData> data_;
};

#endif //EYEONTHEROADCPP_YOLO_DNN_H
