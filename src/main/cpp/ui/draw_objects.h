/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef EYEONTHEROADCPP_DRAW_OBJECTS_H
#define EYEONTHEROADCPP_DRAW_OBJECTS_H

#include <shared_mutex>
#include <opencv2/opencv.hpp>
#include "draw_object.h"

using namespace cv;
using namespace std;

class DrawObjects {

public:
    DrawObjects();
    ~DrawObjects();
    void addObject(const DrawObject& rect);
    queue<DrawObject> getObjects();

private:
    shared_timed_mutex queue_mutex;
    queue<DrawObject> draw_objects;
};

#endif //EYEONTHEROADCPP_DRAW_OBJECTS_H
