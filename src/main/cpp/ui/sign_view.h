/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_SIGN_VIEW_H
#define EYEONTHEROADCPP_SIGN_VIEW_H

#include "opencv2/opencv.hpp"
#include "../utils/road_sign.h"
#include <iostream>

using namespace std;
using namespace cv;

class SignView {

public:
    explicit SignView(const string &resources_dir);
    ~SignView();

    void update();
    void setFrame(Mat &value);
    void showSign(int sign_id);
    void stopRendering();

private:
    const int sign_side = 70;
    const int init_padding_x = 10;
    const int sign_padding_x = 10;
    const int init_padding_y = 70;
    const int sign_padding_y = 10;
    const int display_time_sec = 3;
    Mat frame;
    Mat no_left_turn_sign;
    Mat no_right_turn_sign;
    Mat no_u_turn_sign;
    Mat stop_sign;
    Mat no_parking;
    Mat no_stopping;
    Mat no_trucks;
    Mat speed_limit_5_na;
    Mat speed_limit_10_na;
    Mat speed_limit_15_na;
    Mat speed_limit_20_na;
    Mat speed_limit_25_na;
    Mat speed_limit_30_na;
    Mat speed_limit_35_na;
    Mat speed_limit_40_na;
    Mat speed_limit_45_na;
    Mat speed_limit_50_na;
    Mat speed_limit_55_na;
    Mat speed_limit_60_na;
    Mat speed_limit_65_na;
    Mat speed_limit_70_na;
    Mat speed_limit_75_na;
    Mat speed_limit_80_na;
    Mat speed_limit_85_na;
    Mat speed_limit_90_na;
    Mat speed_limit_95_na;
    Mat speed_limit_100_na;
    Mat speed_limit_110_na;
    Mat speed_limit_120_na;
    vector<int64> show_start_time;
    void showSignInternal();
};

/**
 * Converts value of the speed to appropriate enumeration.
 *
 * @param speed Value of the speed.
 * @return
 */
static inline RoadSign getSpeedLimitSignCanada(const int speed) {
    switch (speed) {
        case 5:
            return SPEED_LIMIT_5_NA;
        case 10:
            return SPEED_LIMIT_10_NA;
        case 15:
            return SPEED_LIMIT_15_NA;
        case 20:
            return SPEED_LIMIT_20_NA;
        case 25:
            return SPEED_LIMIT_25_NA;
        case 30:
            return SPEED_LIMIT_30_NA;
        case 35:
            return SPEED_LIMIT_35_NA;
        case 40:
            return SPEED_LIMIT_40_NA;
        case 45:
            return SPEED_LIMIT_45_NA;
        case 50:
            return SPEED_LIMIT_50_NA;
        case 55:
            return SPEED_LIMIT_55_NA;
        case 60:
            return SPEED_LIMIT_60_NA;
        case 65:
            return SPEED_LIMIT_65_NA;
        case 70:
            return SPEED_LIMIT_70_NA;
        case 75:
            return SPEED_LIMIT_75_NA;
        case 80:
            return SPEED_LIMIT_80_NA;
        case 85:
            return SPEED_LIMIT_85_NA;
        case 90:
            return SPEED_LIMIT_90_NA;
        case 95:
            return SPEED_LIMIT_95_NA;
        case 100:
            return SPEED_LIMIT_100_NA;
        case 110:
            return SPEED_LIMIT_110_NA;
        case 120:
            return SPEED_LIMIT_120_NA;
        default:
            return UNKNOWN;
    }
}

static RoadSign getRoadSign(const int id) {
    switch (id) {
        case 0:
            return NO_LEFT_TURN;
        case 1:
            return NO_RIGHT_TURN;
        case 2:
            return NO_U_TURN;
        case 4:
            return NO_PARKING;
        case 5:
            return NO_STOPPING;
        case 6:
            return NO_TRUCKS;
        default:
            return UNKNOWN;
    }
}

#endif //EYEONTHEROADCPP_SIGN_VIEW_H
