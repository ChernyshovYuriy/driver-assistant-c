/*
 * Copyright 2019-2020 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sign_view.h"
#include "../utils/image_utils.h"
#include "../utils/colors.h"

static Mat addImageToOpaqueFrame( Mat &image, Size &frameSize) {
    Mat frame(frameSize.height, frameSize.width, CV_8UC3, COLOR_white);
    image = resizeImage(image, frameSize);
    const double x = (frame.cols - image.cols) * 1.0 / 2;
    const int y = 0;
    image.copyTo(frame(Rect(static_cast<int>(x), y, image.cols, image.rows)));
    return frame;
}

SignView::SignView(const string &resources_dir) {
    show_start_time.resize(SIGNS_NUMBER);
    Size size(sign_side, sign_side);

    no_left_turn_sign = imread(resources_dir + "no_left_turn.jpg");
    no_right_turn_sign = imread(resources_dir + "no_right_turn.jpg");
    no_u_turn_sign = imread(resources_dir + "no_u_turn.jpg");
    stop_sign = imread(resources_dir + "stop.jpg");
    no_parking = imread(resources_dir + "no_parking.jpg");
    no_stopping = imread(resources_dir + "no_stopping.jpg");
    no_trucks = imread(resources_dir + "no_trucks.jpg");
    speed_limit_5_na = imread(resources_dir + "speed_limit_5_na.jpg");
    speed_limit_10_na = imread(resources_dir + "speed_limit_10_na.jpg");
    speed_limit_15_na = imread(resources_dir + "speed_limit_15_na.jpg");
    speed_limit_20_na = imread(resources_dir + "speed_limit_20_na.jpg");
    speed_limit_30_na = imread(resources_dir + "speed_limit_30_na.jpg");
    speed_limit_40_na = imread(resources_dir + "speed_limit_40_na.jpg");
    speed_limit_50_na = imread(resources_dir + "speed_limit_50_na.jpg");
    speed_limit_60_na = imread(resources_dir + "speed_limit_60_na.jpg");
    speed_limit_70_na = imread(resources_dir + "speed_limit_70_na.jpg");
    speed_limit_80_na = imread(resources_dir + "speed_limit_80_na.jpg");
    speed_limit_90_na = imread(resources_dir + "speed_limit_90_na.jpg");
    speed_limit_100_na = imread(resources_dir + "speed_limit_100_na.jpg");

    speed_limit_5_na = addImageToOpaqueFrame(speed_limit_5_na, size);
    speed_limit_10_na = addImageToOpaqueFrame(speed_limit_10_na, size);
    speed_limit_15_na = addImageToOpaqueFrame(speed_limit_15_na, size);
    speed_limit_20_na = addImageToOpaqueFrame(speed_limit_20_na, size);
    speed_limit_30_na = addImageToOpaqueFrame(speed_limit_30_na, size);
    speed_limit_40_na = addImageToOpaqueFrame(speed_limit_40_na, size);
    speed_limit_50_na = addImageToOpaqueFrame(speed_limit_50_na, size);
    speed_limit_60_na = addImageToOpaqueFrame(speed_limit_60_na, size);
    speed_limit_70_na = addImageToOpaqueFrame(speed_limit_70_na, size);
    speed_limit_80_na = addImageToOpaqueFrame(speed_limit_80_na, size);
    speed_limit_90_na = addImageToOpaqueFrame(speed_limit_90_na, size);
    speed_limit_100_na = addImageToOpaqueFrame(speed_limit_100_na, size);

    resize(no_left_turn_sign, no_left_turn_sign, size);
    resize(no_right_turn_sign, no_right_turn_sign, size);
    resize(no_u_turn_sign, no_u_turn_sign, size);
    resize(stop_sign, stop_sign, size);
    resize(no_parking, no_parking, size);
    resize(no_stopping, no_stopping, size);
    resize(no_trucks, no_trucks, size);
}

SignView::~SignView() {
    no_left_turn_sign.release();
    no_right_turn_sign.release();
    no_u_turn_sign.release();
    no_parking.release();
    no_stopping.release();
    stop_sign.release();
    no_trucks.release();
    show_start_time.clear();
}

void SignView::showSign(int sign_id) {
    show_start_time.at((unsigned long) sign_id) = getTickCount();
    showSignInternal();
}

void SignView::setFrame(Mat &value) {
    this->frame = value;
}

void SignView::stopRendering() {

}

void SignView::showSignInternal() {
    vector<int> displaying_signs;
    int counter = 0;
    for (const int64 start_time : show_start_time) {
        if (start_time != 0) {
            displaying_signs.push_back(counter);
        }
        counter++;
    }
    counter = 0;
    for (const int id : displaying_signs) {
        const int x = init_padding_x + (counter % 2) * (sign_padding_x + sign_side);
        const int y = init_padding_y + (counter / 2) * (sign_padding_y + sign_side);
        const Rect dstRC = Rect(x, y, sign_side, sign_side);
        Mat dstROI = frame(dstRC);
        switch (id) {
            case NO_LEFT_TURN:
                no_left_turn_sign.copyTo(dstROI);
                break;
            case NO_RIGHT_TURN:
                no_right_turn_sign.copyTo(dstROI);
                break;
            case NO_U_TURN:
                no_u_turn_sign.copyTo(dstROI);
                break;
            case STOP_SIGN:
                stop_sign.copyTo(dstROI);
                break;
            case NO_PARKING:
                no_parking.copyTo(dstROI);
                break;
            case NO_TRUCKS:
                no_trucks.copyTo(dstROI);
                break;
            case NO_STOPPING:
                no_stopping.copyTo(dstROI);
                break;
            case SPEED_LIMIT_5_NA:
                speed_limit_5_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_10_NA:
                speed_limit_10_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_15_NA:
                speed_limit_15_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_20_NA:
                speed_limit_20_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_25_NA:

                break;
            case SPEED_LIMIT_30_NA:
                speed_limit_30_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_35_NA:

                break;
            case SPEED_LIMIT_40_NA:
                speed_limit_40_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_45_NA:

                break;
            case SPEED_LIMIT_50_NA:
                speed_limit_50_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_55_NA:

                break;
            case SPEED_LIMIT_60_NA:
                speed_limit_60_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_65_NA:

                break;
            case SPEED_LIMIT_70_NA:
                speed_limit_70_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_75_NA:

                break;
            case SPEED_LIMIT_80_NA:
                speed_limit_80_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_85_NA:

                break;
            case SPEED_LIMIT_90_NA:
                speed_limit_90_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_95_NA:

                break;
            case SPEED_LIMIT_100_NA:
                speed_limit_100_na.copyTo(dstROI);
                break;
            case SPEED_LIMIT_110_NA:

                break;
            case SPEED_LIMIT_120_NA:

                break;
            default:
                break;
        }
        counter++;
        dstROI.release();
    }
    displaying_signs.clear();
}

void SignView::update() {
    frame.setTo(COLOR_main_mat);
    const int64 time = getTickCount();
    int counter = -1;
    for (const int64 start_time : show_start_time) {
        counter++;
        if (start_time == 0) {
            continue;
        }
        const double secs = (time - start_time) / getTickFrequency();
        if (secs <= display_time_sec) {
            showSignInternal();
        } else {
            show_start_time.at((unsigned long) counter) = 0;
        }
    }
}
