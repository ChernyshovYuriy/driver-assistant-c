/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_SPEEDVALUERECOGNITION_H
#define EYEONTHEROADCPP_SPEEDVALUERECOGNITION_H

#include <iostream>
#include "../ocr/digits_ocr.h"
#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;

class SpeedValueRecognition {

public:
    SpeedValueRecognition();

    ~SpeedValueRecognition();

    void init(const string &resources_dir, const string &output_dir);

    int recognize(Mat &roi);

private:
    const double scale_x = 0.85;
    const double scale_y = 0.6;
    string output_dir;
    DigitsOCR digits_ocr;
};

#endif //EYEONTHEROADCPP_SPEEDVALUERECOGNITION_H
