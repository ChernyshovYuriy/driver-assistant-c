#include <utility>

/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "speed_value_recognition.h"
#include "../utils/file_utils.h"
#include "../utils/image_utils.h"
#include "../utils/countor_utils.h"

static inline bool isRectFitsWithin(Rect &outer, Rect &inner) {
    return inner.x >= outer.x && inner.x + inner.width <= outer.x + outer.width
           && inner.y >= outer.y && inner.y + inner.height <= outer.y + outer.height;
}

static inline void adjustRegion(Rect &region, Mat &cropped) {
    const int delta_px = 1;
    const int delta_py = 7;
    region.x -= delta_px;
    if (region.x < 0) {
        region.x = 0;
    }
    region.y -= delta_py;
    if (region.y < 0) {
        region.y = 0;
    }
    region.width += delta_px * 2;
    if (region.x + region.width > cropped.cols) {
        region.width = cropped.cols - region.x;
    }
    region.height += delta_py * 2;
    if (region.y + region.height > cropped.rows) {
        region.height = cropped.rows - region.y;
    }
}

SpeedValueRecognition::SpeedValueRecognition() = default;

SpeedValueRecognition::~SpeedValueRecognition() = default;

void SpeedValueRecognition::init(const string &resources_dir, const string &output_dir) {
    this->output_dir = output_dir;
    digits_ocr.init(const_cast<string &>(resources_dir), const_cast<string &>(output_dir));
}

int SpeedValueRecognition::recognize(Mat &roi) {
    const auto start = getTimestampMilli();
    const auto scale_h = roi.rows / roi.cols;
    // Simple filter of the square shapes, and shapes with width greater the height.
    if (scale_h < 1) {
        return 0;
    }

    // sls - speed limit sign
//    saveMatToFile(roi, output_dir, "sls");
    roi = resizeImage(roi, SIZE_hundred);

    Size size_cropped = roi.size();
    const auto x = (size_cropped.width - static_cast<int>(size_cropped.width * scale_x)) * 0.6;
    const auto y = (size_cropped.height - static_cast<int>(size_cropped.height * scale_y)) * 0.6;
    const Rect rect_cropped(
            static_cast<int>(x), static_cast<int>(y),
            static_cast<int>(size_cropped.width * scale_x), static_cast<int>(size_cropped.height * scale_y)
    );

    Mat cropped = roi(rect_cropped);

//    medianBlur(cropped, cropped, 3);
    threshold(cropped, cropped, 0, 255, THRESH_BINARY_INV | THRESH_OTSU);
//    morphologyEx(cropped, cropped, MORPH_RECT, kernel);

    vector<Vec4i> hierarchy;
    vector<vector<Point>> contours;

    // slsm - speed limit sign modified
//    saveMatToFile(cropped, output_dir, "slsm");

    findContours(cropped.clone(), contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE);
    sort(contours.begin(), contours.end(), LeftRightContourSorter());

    // Vectors of the regions.
    vector<Rect> regions;

    // Reference to the previous region.
    Rect prev_region;

    for (auto &contour : contours) {
        Rect region = boundingRect(contour);

#if DEBUG
        cout << "Region:" << region << " mat:" << cropped.cols << "x" << cropped.rows << endl;
#endif
        // Ignore region less then 20 px height.
        if (region.height < 20) {
#if DEBUG
            cout << " - less then 20px" << endl;
#endif
            continue;
        }
        // Ignore region with a height close to detected sign height.
        if (region.height > cropped.rows * 0.9) {
#if DEBUG
            cout << " - height is too high" << endl;
#endif
            continue;
        }
        // Ignore region with width more then a half of the detected sign
        // (do we need to do it? what about speed limit 5?).
        if (region.width > (cropped.cols >> 1) - 5) {
#if DEBUG
            cout << " - width is to wide" << endl;
#endif
            continue;
        }
        // Ignore region with width less the 5 px
        if (region.width <= 1) {
#if DEBUG
            cout << " - width is to narrow" << endl;
#endif
            continue;
        }
        // Ignore region close to right edge.
        if (region.x >= cropped.cols * 0.8) {
#if DEBUG
            cout << " - x is to close to right side" << endl;
#endif
            continue;
        }
        //TODO:
        // Ignore region close to left edge.
        if (region.x <= cropped.cols - cropped.cols * 0.9 && region.width <= 4) {
#if DEBUG
            //            cout << " - x is to close to left side" << endl;
#endif
            //continue;
        }

        if (isRectFitsWithin(prev_region, region)) {
#if DEBUG
            cout << " - inner region 1" << endl;
#endif
            continue;
        }

        if (isRectFitsWithin(region, prev_region)) {
#if DEBUG
            cout << " - inner region 2" << endl;
#endif
            if (!regions.empty()) {
                regions.pop_back();
            }

            prev_region = Rect(region);
            adjustRegion(region, cropped);
            regions.emplace_back(region);
            continue;
        }

#if DEBUG
        cout << " - passed " << endl;
#endif

        prev_region = Rect(region);
        adjustRegion(region, cropped);
        regions.emplace_back(region);
    }

    string speed_value;
    for (const Rect &region : regions) {
        Mat number_mat = cropped(region);
        auto value = digits_ocr.recognize(number_mat, cropped.type());
#if DEBUG
        cout << "Predicted:" << value << endl;
#endif
        speed_value.append(to_string(value));
    }

    regions.clear();
    cropped.release();

    cout << "Speed:" << speed_value << ", in " << (getTimestampMilli() - start) << " ms" << endl;
    if (speed_value.empty()) {
        speed_value = "0";
    }

//    saveMatToFile(roi, output_dir, "test");
    return stoi(speed_value, nullptr, 10);
}
