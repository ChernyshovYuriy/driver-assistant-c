#ifndef EYEONTHEROADCPP_DNN_H
#define EYEONTHEROADCPP_DNN_H

#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include "../constants.h"

using namespace std;
using namespace cv;
using namespace dnn;

struct dnn_return_data_t {
    string name;
    float probability;
    Rect box;
};

class DNN {

public:

    virtual void init(const string &resources_dir, const string &output_dir) = 0;

    virtual vector<dnn_return_data_t> onFrame(Mat &frame) = 0;

protected:

    const int backend = DNN_BACKEND_CUDA;

    const int target = DNN_TARGET_CUDA;

    /**
     * Confidence threshold.
     */
    const float conf_threshold = 0.5;
    /**
     * Non-maximum suppression threshold.
     */
    const float nms_threshold = 0.5;

    string output_dir;

    Mat scale_to_blob(Mat &frame) {
        if (input_h != inp_size.height) {
            input_h = inp_size.height;
            input_w = inp_size.width;

            // resize the image with scale
            float dim = max(frame.rows, frame.cols);
            resize_h = ((frame.rows / dim) * input_h);
            resize_w = ((frame.cols / dim) * input_w);

            // Additional checks for images with non even dims
            if ((input_w - resize_w) % 2) resize_w--;
            if ((input_h - resize_h) % 2) resize_h--;

            x_offset = (input_w - resize_w) / 2;
            y_offset = (input_h - resize_h) / 2;
        }

        Mat img;
        // resizing
        resize(frame, img, Size(resize_w, resize_h), 0, 0, INTER_CUBIC);
        // letterboxing
        copyMakeBorder(img, img, y_offset, y_offset, x_offset, x_offset,
                       BORDER_CONSTANT, Scalar(128, 128, 128));

        return img;
    }

private:
    int input_h = 0;
    int input_w = 0;
    int resize_h = 0;
    int resize_w = 0;
    int x_offset = 0;
    int y_offset = 0;
};

#endif //EYEONTHEROADCPP_DNN_H
