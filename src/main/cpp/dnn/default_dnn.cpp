/*
 * Copyright 2019-2020 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "darknet.h"
#include "default_dnn.h"
#include <memory>
#include <fstream>

using namespace cv;
using namespace dnn;

struct DNNInternalData {
    network *darknet = nullptr;
    Net net;
    vector<string> out_names;
    vector<string> classes;
};

DefaultDNN::DefaultDNN() {
    data_ = make_unique<DNNInternalData>();
}

DefaultDNN::~DefaultDNN() = default;

void DefaultDNN::init(const string &resources_dir, const string &output_dir) {
    ifstream obj_names_file;
    obj_names_file.open(resources_dir + obj_names);
    string line;
    cout << "Classes:" << endl;
    while (getline(obj_names_file, line)) {
        data_->classes.push_back(line);
        cout << " - " << line << endl;
    }
    obj_names_file.close();

    data_->net = readNet(
            resources_dir + config_name,
            resources_dir + model_name,
            NET_NAME
    );
    data_->net.setPreferableBackend(backend);
    data_->net.setPreferableTarget(target);
    data_->out_names = data_->net.getUnconnectedOutLayersNames();

    this->output_dir = output_dir;

    string cfg_file_str = resources_dir + config_name;
    string weight_file_str = resources_dir + model_name;
    char *cfg_file = const_cast<char *>(cfg_file_str.c_str());
    char *weight_file = const_cast<char *>(weight_file_str.c_str());
    data_->darknet = load_network(cfg_file, weight_file, 0);
}

vector<dnn_return_data_t> DefaultDNN::onFrame(Mat &frame) {
//    cv::Mat top_left
//            = frame(cv::Range(0, frame.rows / 2 - 1), cv::Range(0, frame.cols / 2 - 1));
//    Mat top_right = frame(Range(0, (frame.rows >> 1) - 1), Range(frame.cols >> 1, frame.cols - 1));
//    cv::Mat bottom_left
//            = frame(cv::Range(frame.rows / 2, frame.rows - 1), cv::Range(0, frame.cols / 2 - 1));
//    cv::Mat bottom_right
//            = frame(cv::Range(frame.rows / 2, frame.rows - 1), cv::Range(frame.cols / 2, frame.cols - 1));

    //////////////////////////
    // Currently unstable :-(
    //////////////////////////

    Mat blob;
    blobFromImage(frame, blob, blob_scale, inp_size, blob_scalar, true, false, CV_8U);

    // Run a model.
    data_->net.setInput(blob, net_input_name, net_scale, net_mean);

    vector<Mat> outs;
    data_->net.forward(outs, data_->out_names);

    vector<int> class_ids;
    vector<float> confidences;
    vector<Rect> boxes;
    for (const auto &out : outs) {
        //Network produces output blob with a shape NxC where N is a number of
        //detected objects and C is a number of classes + 4 where the first 4
        //numbers are [center_x, center_y, width, height]
        auto *data = (float *) out.data;
        for (int j = 0; j < out.rows; ++j, data += out.cols) {
            Mat scores = out.row(j).colRange(5, out.cols);
            Point classIdPoint;
            double confidence;
            minMaxLoc(scores, nullptr, &confidence, nullptr, &classIdPoint);
            if (confidence > conf_threshold) {
                int centerX = (int) (data[0] * frame.cols);
                int centerY = (int) (data[1] * frame.rows);
                int w = (int) (data[2] * frame.cols);
                int h = (int) (data[3] * frame.rows);
                int x = centerX - w / 2;
                int y = centerY - h / 2;

                if (x < 0) x = 0;
                if (x > frame.cols) x = frame.cols;
                if (y < 0) y = 0;
                if (y > frame.rows) y = frame.rows;
                if (x + w > frame.cols) w = frame.cols - x;
                if (y + h > frame.rows) h = frame.rows - y;

                class_ids.push_back(classIdPoint.x);
                confidences.push_back((float) confidence);
                boxes.emplace_back(x, y, w, h);
            }
        }
    }

    vector<int> indices;
    vector<dnn_return_data_t> result;
    NMSBoxes(boxes, confidences, conf_threshold, nms_threshold, indices);
    for (const int idx : indices) {
        result.push_back({data_->classes[class_ids[idx]], confidences[idx], boxes[idx]});
    }

    return result;
}
