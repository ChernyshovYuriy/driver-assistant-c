/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_RECOGNIZE_SIGN_TEST_H
#define EYEONTHEROADCPP_RECOGNIZE_SIGN_TEST_H

#include <string>
#include <dirent.h>
#include <opencv2/opencv.hpp>
#include "../dnn/default_dnn.h"
#include "../utils/image_utils.h"
#include "../utils/colors.h"
#include "../processor/frame_processor.h"

using namespace std;
using namespace cv;

void testProcessFrame(const string &output_dir, string &img_path, FrameProcessor &processor,
        DrawObjects &draw_objects) {
    auto start = getTimestampMilli();
    Mat src = imread(img_path);
    src = resizeImage(src, SIZE_high_res);
    for (int i = 0; i < 1; ++i) {
//        processor.onFrame(src);
        processor.process(src);
    }
    cout << "TestRecognize frame processed in " << (getTimestampMilli() - start) << " ms" << endl;

    queue<DrawObject> rects = draw_objects.getObjects();
    bool found = false;
    while (!rects.empty()) {
        rectangle(src, rects.front().rect, COLOR_red, 2);
        rects.pop();
        found = true;
    }
    if (found) {
        saveMatToFile(src, output_dir, "dnn");
    }

    src.release();
}

void testRecognizeSpeedInDir(string &root_dir, int speed) {
    auto start = getTimestampMilli();
//    auto *speedSign_processor = dynamic_cast<SpeedLimitSignProcessor *>(processor);
    DIR *dir;
    struct dirent *ent;
    const string speed_str = to_string(speed);
    const string images_dir(root_dir + "src/main/cpp/test/detected/" + speed_str + "/");
    int passed = 0;
    int failed = 0;
    if ((dir = opendir(images_dir.c_str())) != nullptr) {
        while ((ent = readdir(dir)) != nullptr) {
            if (strEndWith(ent->d_name, ".jpg") == 0) {
                continue;
            }
            const string img_path(images_dir + ent->d_name);
            Mat src = imread(img_path, IMREAD_GRAYSCALE);
//            const string speed_detected = speedSign_processor->recognizeSpeed(src);
//            if (speed_detected != speed_str) {
//                cerr << "Image:" << img_path << " Speed:" << speed_detected << endl;
//                failed++;
//            } else {
//                passed++;
//            }
        }
        closedir(dir);
    }
    cout << "TestRecognizeSpeed processed in " << (getTimestampMilli() - start) << " ms" << endl;
    cout << passed << " passed, " << failed << " failed" << endl;
}

void testRecognizeSpeedInFile(string &root_dir) {
    auto start = getTimestampMilli();
//    auto *speedLimitSignProcessor = dynamic_cast<SpeedLimitSignProcessor *>(processor);
    string img_path(root_dir + "src/main/cpp/test/detected/100/24.jpg");
    Mat src = imread(img_path);
//    const string speed = speedLimitSignProcessor->recognizeSpeed(src);
//    if (speed != "100") {
//        cerr << "Image:" << img_path << " Speed:" << speed << endl;
//    } else {
//        cout << "Image:" << img_path << " Speed:" << speed << endl;
//    }
    cout << "TestRecognizeSpeed processed in " << (getTimestampMilli() - start) << " ms" << endl;
}

#endif //EYEONTHEROADCPP_RECOGNIZE_SIGN_TEST_H
