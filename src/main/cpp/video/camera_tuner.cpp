/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "camera_tuner.h"

CameraTuner::CameraTuner(VideoCapture &video_capture, string &frame_name) : video_capture(video_capture) {

    cap_brightness = (float) video_capture.get(CAP_PROP_BRIGHTNESS);
    cap_contrast = (float) video_capture.get(CAP_PROP_CONTRAST);
    cap_saturation = (float) video_capture.get(CAP_PROP_SATURATION);
    cap_gain = (float) video_capture.get(CAP_PROP_GAIN);
    cap_hue = (float) video_capture.get(CAP_PROP_HUE);

    brightness_value = int(cap_brightness * 100);
    contrast_value = int(cap_contrast * 100);
    saturation_value = int(cap_saturation * 100);
    gain_value = int(cap_gain * 100);
    hue_value = int(cap_hue * 100);

    createTrackbar("Brightness", frame_name, &brightness_value, 100, onTrackbarChanged, this);
    createTrackbar("Contrast", frame_name, &contrast_value, 100, onTrackbarChanged, this);
    createTrackbar("Saturation", frame_name, &saturation_value, 100, onTrackbarChanged, this);
    createTrackbar("Gain", frame_name, &gain_value, 100, onTrackbarChanged, this);
    createTrackbar("Hue", frame_name, &hue_value, 100, onTrackbarChanged, this);
}
