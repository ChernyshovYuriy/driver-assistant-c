/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "video_capture_manager.h"
#include "../utils/string_utils.h"
#include "../utils/image_utils.h"
#include "../constants.h"

VideoCaptureManager::VideoCaptureManager() {
    width = 0;
    height = 0;
}

VideoCaptureManager::~VideoCaptureManager() {
    closeStream();
}

int VideoCaptureManager::openStream(const string &src) {
    string src_copy = src;
    trim(src_copy);
    int api_id = cv::CAP_ANY; // 0 = autodetect default API
    if (is_integer(src_copy)) {
        //TODO: Implement camera selection via UI and refactor this code to separate class
        // Select Video Camera attached to device
        // (0 - embedded, 1 - external, 2 - could be another external ... etc ...).
        int device_id = static_cast<int>(strtol(src_copy.c_str(), nullptr, 10));
        cap.open(device_id + api_id);
    } else {
        cap.open(src_copy);
    }
    if (!cap.isOpened()) {
        cerr << "Camera can not be opened: " << src_copy << endl;
        return -1;
    }
    return 0;
}

void VideoCaptureManager::closeStream() {
    // The camera should be de-initialized automatically in VideoCapture destructor.
    cap.release();
}

void VideoCaptureManager::handleInitSize(const string &video_size) {
    double init_w = cap.get(CAP_PROP_FRAME_WIDTH);
    double init_h = cap.get(CAP_PROP_FRAME_HEIGHT);
    cout << "Video frame init dimensions " << init_w << "x" << init_h << endl;

    const unsigned long dlmt_pos = video_size.find(x_label);
    width = static_cast<int>(strtol(video_size.substr(0, dlmt_pos).c_str(), nullptr, 10));
    height = static_cast<int>(
            strtol(video_size.substr(dlmt_pos + 1, video_size.length() - dlmt_pos + 1).c_str(), nullptr, 10)
        );
    cout << "Try to change video frame to " << width << "x" << height << endl;

    bool result_w = cap.set(CAP_PROP_FRAME_WIDTH, width);
    bool result_h = cap.set(CAP_PROP_FRAME_HEIGHT, height);
    cout << "Video frame dimensions are " << ((result_w && result_h) ? "changed" : "not changed") << endl;

//    if (width > this->width || height > this->height) {
//        const float factor = (this->width * 1.0F) / width;
//        scaled_size.width = this->width;
//        scaled_size.height = static_cast<int>(height * factor);
//        cout << "Scaled size " << scaled_size << endl;
//    } else {
        scaled_size.width = width;
        scaled_size.height = height;
//    }
}

void VideoCaptureManager::getFrame(Mat &mat) {
    cap >> mat;
    if (mat.cols > width || mat.rows > height) {
//        const auto start = getTimestampMilli();
        mat = resizeImage(mat, scaled_size);
//        cout << "Frame scaled from " << mat.cols << "x" << mat.rows << " to " << width << "x" << height << " in "
//             << (getTimestampMilli() - start) << " ms" << endl;
    }
}

bool VideoCaptureManager::isOpened() {
    return cap.isOpened();
}

int VideoCaptureManager::getWidth() const {
    return width;
}

int VideoCaptureManager::getHeight() const {
    return height;
}
