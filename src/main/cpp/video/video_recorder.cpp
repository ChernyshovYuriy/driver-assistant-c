/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "video_recorder.h"
#include "../utils/time_utils.h"

VideoRecorder::VideoRecorder(string output_dir, int frame_width, int frame_height) {
    video_size.width = frame_width;
    video_size.height = frame_height;
    this->output_dir = move(output_dir);
}

VideoRecorder::~VideoRecorder() {
    //cout << "Destruct " << typeid(this).name() << endl;
    stop();
}

void VideoRecorder::start(Mat &input) {
    if (video.isOpened()) {
        video.write(input);
        return;
    }

    const string file_name(output_dir + "/video_" + to_string(getTimestampNano()) + ".avi");
    // List of available codecs for CV_FOURCC: http://www.fourcc.org/codecs.php
    int codec = VideoWriter::fourcc('M', 'J', 'P', 'G');
    video.open(file_name, codec, 30, video_size, true);
}

void VideoRecorder::stop() {
    if (video.isOpened()) {
        video.release();
    }
}
