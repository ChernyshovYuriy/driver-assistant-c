/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_VIDEORECORDER_H
#define EYEONTHEROADCPP_VIDEORECORDER_H

#include <string>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

class VideoRecorder {

public:
    explicit VideoRecorder(string output_dir, int frame_width, int frame_height);
    ~VideoRecorder();

    void start(Mat &input);
    void stop();

private:
    string output_dir;
    Size video_size;
    VideoWriter video;
};

#endif //EYEONTHEROADCPP_VIDEORECORDER_H
