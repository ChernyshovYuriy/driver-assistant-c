/*
 * Copyright 2019 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EYEONTHEROADCPP_CAMERATUNER_H
#define EYEONTHEROADCPP_CAMERATUNER_H

#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <opencv2/opencv.hpp>

#define CAMERA_BASE_NAME "/dev/video"

using namespace cv;
using namespace std;

class CameraTuner {

    static void onTrackbarChanged(int new_value, void *object) {
        auto *cameraTuner = (CameraTuner *) object;

        cameraTuner->cap_brightness = float(cameraTuner->brightness_value) / 100;
        cameraTuner->cap_contrast = float(cameraTuner->contrast_value) / 100;
        cameraTuner->cap_saturation = float(cameraTuner->saturation_value) / 100;
        cameraTuner->cap_gain = float(cameraTuner->gain_value) / 100;
        cameraTuner->cap_hue = float(cameraTuner->hue_value) / 100;

        cameraTuner->video_capture.set(CAP_PROP_BRIGHTNESS, cameraTuner->cap_brightness);
        cameraTuner->video_capture.set(CAP_PROP_CONTRAST, cameraTuner->cap_contrast);
        cameraTuner->video_capture.set(CAP_PROP_SATURATION, cameraTuner->cap_saturation);
        cameraTuner->video_capture.set(CAP_PROP_GAIN, cameraTuner->cap_gain);
        cameraTuner->video_capture.set(CAP_PROP_HUE, cameraTuner->cap_hue);
    }

public:
    CameraTuner(VideoCapture &video_capture, string &frame_name);

private:
    VideoCapture &video_capture;

    float cap_brightness;
    float cap_contrast;
    float cap_saturation;
    float cap_gain;
    float cap_hue;

    int brightness_value;
    int contrast_value;
    int saturation_value;
    int gain_value;
    int hue_value;
};

static vector<int> getNumOfCameras() {
        // This is really arbitrary number, it can be any number and depends on how many cameras connected to computer.
        int check_number = 5;
        vector<int> cameras;
        for (int i = 0; i < check_number; ++i) {
                const string name(CAMERA_BASE_NAME + to_string(i));
                const int fd = open(name.c_str(), O_RDONLY);
                const int res = close(fd);
//                cout << "Camera '" << name << "' open status:" << fd << " close status:" << res << endl;
                if (fd != -1) {
                        cameras.push_back(i);
                }
        }
        return cameras;
}

#endif //EYEONTHEROADCPP_CAMERATUNER_H
