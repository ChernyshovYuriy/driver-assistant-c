import argparse
import os
import random
import time
from random import randint

import cv2
import numpy
import numpy as np
from PIL import Image, ImageFile

from utils import pil_image_resize

########################################################################################################################
# Command line arguments.
########################################################################################################################

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-s", "--src", required=True,
                help="Source directory of images to be processed. Have to be PNG images with transparent background.")
ap.add_argument("-d", "--dst", required=True,
                help="Destination directory to save processed images.")
args = vars(ap.parse_args())

########################################################################################################################
# Custom set up.
########################################################################################################################

# Source directory of images to be processed. Have to be PNG images with transparent background.
src = args["src"]
print("Src: {}".format(src))
# Destination directory to save processed images.
dst = args["dst"]
print("Dst: {}".format(dst))
# Number of mutations for each image from "src".
mutations = 500
# Min and Max value to use when scale image on each iteration. Actual value will be randomly generated based on this
# range.
dimension = [30, 200]
# Modify these variables to tune image distortions:
# Min and Max value of geometric distortion of object in percentage of its original dimensions.
# Actual value will be randomly generated based on this range.
shift_arr = [5, 25]
# Min and Max value of gamma distortion of object.
# Actual value will be randomly generated based on this range.
gamma_arr = [0.1, 1]
# Min and Max value of fake light distortion of object.
# Actual value will be randomly generated based on this range.
fake_light_arr = [1, 8]
# Array of angles to use to rotate single target image in order to create diverse set.
angles = [Image.ROTATE_90, Image.ROTATE_270]

########################################################################################################################
# Project logic.
########################################################################################################################

file_ext = ".png"
num = int(mutations / len(os.listdir(src)))
imgs_total = num * len(os.listdir(src))

if not os.path.exists(dst):
    os.makedirs(dst)


def find_coeffs(pa, pb):
    matrix = []
    for p1, p2 in zip(pa, pb):
        matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0] * p1[0], -p2[0] * p1[1]])
        matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1] * p1[0], -p2[1] * p1[1]])

    a = numpy.matrix(matrix, dtype=numpy.float)
    b = numpy.array(pb).reshape(8)

    res = numpy.dot(numpy.linalg.inv(a.T * a) * a.T, b)
    return numpy.array(res).reshape(8)


def get_saturation(value, quadrant):
    """
    Return color value depending on quadrant and saturation.
    :param value:
    :param quadrant:
    :return:
    """
    if value > 223:
        return 255
    elif value > 159:
        if quadrant != 1:
            return 255

        return 0
    elif value > 95:
        if quadrant == 0 or quadrant == 3:
            return 255

        return 0
    elif value > 32:
        if quadrant == 1:
            return 255

        return 0
    else:
        return 0


def convert_dithering(image):
    """
    Create a dithered version of the image.
    :param image:
    :return:
    """
    # Get size
    img_width = image.width
    img_height = image.height

    # Create new Image and a Pixel Map
    new = create_image(img_width, img_height)
    pixels = new.load()

    # Transform to half tones
    for i in range(0, img_width, 2):
        for j in range(0, img_height, 2):
            # Get Pixels
            p1 = get_pixel(image, i, j)
            p2 = get_pixel(image, i, j + 1)
            p3 = get_pixel(image, i + 1, j)
            p4 = get_pixel(image, i + 1, j + 1)

            # Color Saturation by RGB channel
            red = (p1[0] + p2[0] + p3[0] + p4[0]) / 4
            green = (p1[1] + p2[1] + p3[1] + p4[1]) / 4
            blue = (p1[2] + p2[2] + p3[2] + p4[2]) / 4

            # Results by channel
            r = [0, 0, 0, 0]
            g = [0, 0, 0, 0]
            b = [0, 0, 0, 0]

            # Get Quadrant Color
            for x in range(0, 4):
                r[x] = get_saturation(red, x)
                g[x] = get_saturation(green, x)
                b[x] = get_saturation(blue, x)

            # Set Dithered Colors
            pixels[i, j] = (r[0], g[0], b[0])
            pixels[i, j + 1] = (r[1], g[1], b[1])
            pixels[i + 1, j] = (r[2], g[2], b[2])
            pixels[i + 1, j + 1] = (r[3], g[3], b[3])

        # Return new image
        return new


def get_pixel(image, i, j):
    """
    Get the pixel from the given image
    :param image:
    :param i:
    :param j:
    :return:
    """
    # Inside image bounds?
    img_width, img_height = image.size
    if i > img_width or j > img_height:
        return None

    # Get Pixel
    pixel = image.getpixel((i, j))
    return pixel


def create_image(i, j):
    """
    Create a new image with the given size.
    :param i:
    :param j:
    :return:
    """
    image = Image.new("RGBA", (i, j), (0, 0, 0, 0))
    # image = Image.new("RGB", (i, j), "white")
    return image


def convert_halftoning(image):
    """
    Create a Half-tone version of the image.
    :param image:
    :return:
    """
    # Get size
    img_width, img_height = image.size

    # Create new Image and a Pixel Map
    new = create_image(img_width, img_height)
    pixels = new.load()

    # Transform to half tones
    for i in range(0, img_width, 2):
        for j in range(0, img_height, 2):
            # Get Pixels
            p1 = get_pixel(image, i, j)
            p2 = get_pixel(image, i, j + 1)
            p3 = get_pixel(image, i + 1, j)
            p4 = get_pixel(image, i + 1, j + 1)

            # Transform to grayscale
            gray1 = (p1[0] * 0.299) + (p1[1] * 0.587) + (p1[2] * 0.114)
            gray2 = (p2[0] * 0.299) + (p2[1] * 0.587) + (p2[2] * 0.114)
            gray3 = (p3[0] * 0.299) + (p3[1] * 0.587) + (p3[2] * 0.114)
            gray4 = (p4[0] * 0.299) + (p4[1] * 0.587) + (p4[2] * 0.114)

            # Saturation Percentage
            sat = (gray1 + gray2 + gray3 + gray4) / 4

            # Draw white/black depending on saturation
            if sat > 223:
                pixels[i, j] = (255, 255, 255)  # White
                pixels[i, j + 1] = (255, 255, 255)  # White
                pixels[i + 1, j] = (255, 255, 255)  # White
                pixels[i + 1, j + 1] = (255, 255, 255)  # White
            elif sat > 159:
                pixels[i, j] = (255, 255, 255)  # White
                pixels[i, j + 1] = (0, 0, 0)  # Black
                pixels[i + 1, j] = (255, 255, 255)  # White
                pixels[i + 1, j + 1] = (255, 255, 255)  # White
            elif sat > 95:
                pixels[i, j] = (255, 255, 255)  # White
                pixels[i, j + 1] = (0, 0, 0)  # Black
                pixels[i + 1, j] = (0, 0, 0)  # Black
                pixels[i + 1, j + 1] = (255, 255, 255)  # White
            elif sat > 32:
                pixels[i, j] = (0, 0, 0)  # Black
                pixels[i, j + 1] = (255, 255, 255)  # White
                pixels[i + 1, j] = (0, 0, 0)  # Black
                pixels[i + 1, j + 1] = (0, 0, 0)  # Black
            else:
                pixels[i, j] = (0, 0, 0)  # Black
                pixels[i, j + 1] = (0, 0, 0)  # Black
                pixels[i + 1, j] = (0, 0, 0)  # Black
                pixels[i + 1, j + 1] = (0, 0, 0)  # Black

        # Return new image
        return new


def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    inv_gamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** inv_gamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")

    # apply gamma correction using the lookup table
    return Image.fromarray(
        cv2.cvtColor(cv2.LUT(numpy.array(image), table), cv2.COLOR_mRGBA2RGBA)
    )


def fake_light(image, tilesize=50):
    image_width, image_height = image.size
    for x in range(0, image_width, tilesize):
        for y in range(0, image_height, tilesize):
            br = int(255 * (1 - x / float(image_width) * y / float(image_height)))
            tile = Image.new("RGBA", (tilesize, tilesize), (255, 255, 255, 128))
            image.paste((br, br, br), (x, y, x + tilesize, y + tilesize), mask=tile)


def get_coeffs(width_in, height_in):
    global shift_arr
    shift_w = randint(int(width_in * shift_arr[0] / 100.0), int(width_in * shift_arr[1] / 100.0))
    shift_h = randint(int(height_in * shift_arr[0] / 100.0), int(height_in * shift_arr[1] / 100.0))
    num_of_coeff_variants = 3
    coeff_id = randint(0, num_of_coeff_variants)
    if coeff_id == 0:
        """
        Transform right side in perspective
        """
        coeffs = find_coeffs(
            [(0, 0), (width_in, 0), (width_in, height_in), (0, height_in)],
            [(0, 0), (width_in + shift_w, -shift_h), (width_in + shift_w, height_in + shift_h), (0, height_in)])
    elif coeff_id == 1:
        """
        Transform left side in perspective
        """
        coeffs = find_coeffs(
            [(0, 0), (width_in, 0), (width_in, height_in), (0, height_in)],
            [(-shift_w, -shift_h), (width_in, 0), (width_in, height_in), (-shift_w, height_in + shift_h)])
    elif coeff_id == 2:
        """
        Transform top side in perspective
        """
        coeffs = find_coeffs(
            [(0, 0), (width_in, 0), (width_in, height_in), (0, height_in)],
            [(-shift_w, -shift_h), (width_in + shift_w * 2, -shift_h), (width_in, height_in), (0, height_in)])
    else:
        """
        No transform
        """
        coeffs = find_coeffs(
            [(0, 0), (width_in, 0), (width_in, height_in), (0, height_in)],
            [(0, 0), (width_in, 0), (width_in, height_in), (0, height_in)])
    return coeffs


print("Start images transformation ...")

# Use this to avoid IOError: image file is truncated (N bytes not processed)
ImageFile.LOAD_TRUNCATED_IMAGES = True
count = 0
for file_name in os.listdir(src):
    if not file_name.endswith(file_ext):
        print("File is not PNG image:", file_name)
        continue
    for i in range(0, num):
        img = Image.open(os.path.join(src, file_name))
        img = pil_image_resize(img, randint(dimension[0], dimension[1]))
        img = img.transform(
            (img.width, img.height), Image.PERSPECTIVE, get_coeffs(img.width, img.height), Image.BICUBIC
        )
        img = img.convert("RGBA")
        img = img.crop(img.getbbox())

        count += 1

        if count % 3 == 0:
            img_origin = img.copy()
            fake_light(img, randint(fake_light_arr[0], fake_light_arr[1]))
            img = Image.composite(img, img_origin, img_origin)

        img = adjust_gamma(img, random.uniform(gamma_arr[0], gamma_arr[1]))
        if count % 3 == 0:
            img = img.transpose(method=angles[randint(0, 1)])

        file_name_new = str(int(round(time.time() * 1000))) + file_ext
        img.save(os.path.join(dst, file_name_new), "PNG", compress_level=0, optimize=False)

        # print("Image {} of {} transformed".format(count, imgs_total))

print("{} images transformed".format(count))
