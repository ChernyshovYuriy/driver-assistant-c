import os
import sys
from os.path import dirname, abspath

import cv2

from utils import cv_image_resize

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

src = "/home/tstone10/dev/yolo/bg/"
w = 416
h = 416
file_ext_png = ".png"
file_ext_jpeg = ".jpeg"
file_ext_jpg = ".jpg"

print("Resize images to max of ", str(w), str(h), "at", src)

counter = 1
for filename in os.listdir(src):
    if filename.endswith(file_ext_png) or filename.endswith(file_ext_jpeg) or filename.endswith(file_ext_jpg):
        img = cv2.imread(os.path.join(src, filename), cv2.IMREAD_UNCHANGED)
        img_rows, img_cols = img.shape[:2]
        resized_img = cv_image_resize(img, w, h, cv2.INTER_AREA, False)
        p = os.path.sep.join((src, filename))
        cv2.imwrite(p, resized_img)
        resized_img_rows, resized_img_cols = resized_img.shape[:2]
        print("%d [%d x %d] -> [%d x %d]" % (counter, img_cols, img_rows, resized_img_cols, resized_img_rows))
        counter += 1
    else:
        print("File %s is not an image" % filename)
        continue

print("{} images scaled".format(counter))
