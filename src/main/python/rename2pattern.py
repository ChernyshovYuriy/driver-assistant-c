import os

from PIL import Image, ImageFile

src = "/home/tstone10/dev/yolo/bg_non_labeled/01"
ext = ".png"
pattern = "bg_non_labeled_"
idx = 0

# Use this to avoid IOError: image file is truncated (N bytes not processed)
ImageFile.LOAD_TRUNCATED_IMAGES = True

counter = 0
for file_name in os.listdir(src):
    if file_name.endswith(ext):
        img = Image.open(os.path.join(src, file_name)).convert("RGBA")
        p = os.path.sep.join((src, file_name))
        os.remove(p)
        file_name_new = pattern + str(idx) + ext
        file_name_full = os.path.join(src, file_name_new)
        img.save(
            file_name_full,
            "PNG", compress_level=0, optimize=False
        )
        idx += 1
        counter += 1
        print("Image", file_name, "converted to", file_name_new)
    else:
        print("File is not PNG image:", file_name)
        continue

print("{} images converted".format(counter))
