# -------------------------------------------------------------------------------
# Name:        Object bounding box label tool
# Purpose:     Label object bboxes for ImageNet Detection data
# Author:      Qiushi
# Created:     06/06/2014
#
# NOTE:
# Inspired by Qiushi's code, this is a tool to prepare data for the YOLO
# framework to train.
# -------------------------------------------------------------------------------

from __future__ import division

import os
import shelve
from tkinter import *
from tkinter import filedialog
from tkinter.messagebox import showinfo, showerror

import cv2
import imutils
from PIL import Image

# colors for the boundary boxes
COLORS = ['red', 'blue', 'yellow', 'pink', 'cyan', 'green', 'black']


class LabelTool():
    def __init__(self, master):
        # set up the main frame
        self.window_label = "LabelTool"
        master.title(self.window_label)
        frame = Frame(master)
        frame.pack(fill=BOTH, expand=1)
        master.resizable(width=FALSE, height=FALSE)

        # initialize global state
        self.images_names = []
        self.data_dir = ''
        self.cur_img_id = 0
        self.total_imgs = 0
        self.image_name = ''
        self.tk_img = None
        self.cur_img = None
        self.cur_img_cv2 = None
        self.images_dir = ""

        # storage
        self.storage_file_name = "bbt_cache"
        self.storage_imgs_dir_key = "storage_imgs_dir_key"

        # initialize mouse state
        self.state = {'click': 0, 'x': 0, 'y': 0}

        # reference to bbox
        self.bbox_id_list = []
        self.bbox_id = None
        self.boxes_list = []
        self.hl = None
        self.vl = None

        # ----------------- GUI stuff ---------------------
        # dir entry & load
        label = Label(frame, text="Image Dir:")
        label.grid(row=0, column=0, sticky=E)
        label = Label(frame, text="Category id:")
        label.grid(row=1, column=0, sticky=E)
        self.data_dir_entry = Entry(frame)
        self.data_dir_entry.grid(row=0, column=1, sticky=W + E)
        self.category_entry = Entry(frame)
        self.category_entry.grid(row=1, column=1, sticky=W + E)
        browse_btn = Button(frame, text="Browse", command=self.browse_dir)
        browse_btn.grid(row=0, column=2, sticky=W + E)

        # main panel for labeling
        self.main_panel = Canvas(frame, cursor='tcross')
        self.main_panel.bind("<Button-1>", self.mouse_click)
        self.main_panel.bind("<Motion>", self.mouse_move)
        master.bind("<Key>", self.on_key_event)
        self.main_panel.grid(row=2, column=1, rowspan=4, sticky=W + N)

        # showing bbox info & delete bbox
        bboxes_label = Label(frame, text='Bounding boxes:')
        bboxes_label.grid(row=1, column=2, sticky=W + N)
        self.list_box = Listbox(frame, width=22, height=12)
        self.list_box.grid(row=2, column=2, sticky=N)
        del_btn = Button(frame, text='Delete', command=self.del_bbox)
        del_btn.grid(row=3, column=2, sticky=W + E + N)
        clear_btn = Button(frame, text='ClearAll', command=self.clear_bbox)
        clear_btn.grid(row=4, column=2, sticky=W + E + N)

        # control panel for image navigation
        ctr_panel = Frame(frame)
        ctr_panel.grid(row=5, column=1, columnspan=2, sticky=W + E)
        prev_btn = Button(ctr_panel, text='<< Prev', width=10, command=self.prev_image)
        prev_btn.pack(side=LEFT, padx=5, pady=3)
        self.save_btn_text = StringVar()
        self.save_btn_text.set("Save")
        save_btn = Button(ctr_panel, textvariable=self.save_btn_text, width=10, command=self.save_data)
        save_btn.pack(side=LEFT, padx=5, pady=3)
        next_btn = Button(ctr_panel, text='Next >>', width=10, command=self.next_image)
        next_btn.pack(side=LEFT, padx=5, pady=3)
        self.prog_label = Label(ctr_panel, text="Progress: / ")
        self.prog_label.pack(side=LEFT, padx=5)
        tmp_label = Label(ctr_panel, text="Go to image no.")
        tmp_label.pack(side=LEFT, padx=5)
        self.idx_entry = Entry(ctr_panel, width=5)
        self.idx_entry.pack(side=LEFT)
        go_to_btn = Button(ctr_panel, text='Go', command=self.goto_image)
        go_to_btn.pack(side=LEFT)

        frame.columnconfigure(1, weight=1)
        frame.rowconfigure(4, weight=1)

        self.images_dir = self.load_cache(self.storage_imgs_dir_key)
        if self.images_dir is None:
            self.images_dir = ""
        else:
            self.load_dir()
        self.data_dir_entry.delete(0, END)
        self.data_dir_entry.insert(0, self.images_dir)

    def on_key_event(self, event):
        key_code = event.keycode
        print("Key event, key code %d" % key_code)
        if key_code == 36:
            # print("Enter pressed")
            self.save_data()
            pass
        elif key_code == 113:
            # print("< pressed")
            self.prev_image()
            pass
        elif key_code == 114:
            # print("> pressed")
            self.next_image()
            pass

    def browse_dir(self):
        """
        Browse images directory.
        :return:
        """
        self.images_dir = filedialog.askdirectory()
        self.data_dir_entry.delete(0, END)
        self.data_dir_entry.insert(0, self.images_dir)
        self.save_cache(self.storage_imgs_dir_key, self.images_dir)
        self.load_dir()

    def load_dir(self):
        """
        Load data into memory.
        :return:
        """
        # get list of images
        for filename in os.listdir(self.images_dir):
            if not (filename.endswith(".jpg") or filename.endswith(".jpeg") or filename.endswith(".png")):
                continue
            self.images_names.append(filename)

        self.total_imgs = len(self.images_names)
        if self.total_imgs == 0:
            showinfo(self.window_label, 'No images found in the specified dir')
            return -1

        # set up labels dir
        self.data_dir = self.images_dir + "/data"
        if not os.path.exists(self.data_dir):
            os.mkdir(self.data_dir)

        print('%d images loaded from %s' % (self.total_imgs, self.images_dir))
        self.load_current_image()

    def load_current_image(self):
        # load image
        image_path = self.images_dir + "/" + self.images_names[self.cur_img_id]
        print("Load cur img, id:%d, path:%s" % (self.cur_img_id, image_path))
        self.cur_img = Image.open(image_path)
        self.cur_img_cv2 = cv2.imread(image_path)
        self.tk_img = Image.PhotoImage(self.cur_img)
        self.main_panel.config(width=max(self.tk_img.width(), 400), height=max(self.tk_img.height(), 400))
        self.main_panel.create_image(0, 0, image=self.tk_img, anchor=NW)
        self.prog_label.config(text="Progress:%03d/%03d" % (self.cur_img_id + 1, self.total_imgs))

        # load labels
        self.clear_bbox()
        # TODO: Need to read only image with 0 degree rotation
        # self.image_name = os.path.split(image_path)[-1].split('.')[0]
        # label_name = self.image_name + '.txt'
        # label_file_name = os.path.join(self.data_dir, label_name)
        # print("Label file name %s" % label_file_name)
        # if os.path.exists(label_file_name):
        #     with open(label_file_name) as f:
        #         for (i, line) in enumerate(f):
        #             tmp = [float(t.strip()) for t in line.split()]
                    # TODO: Need to convert back from YOLO format to coordinates
                    # self.bbox_list.append(tuple(tmp))
                    # tmp_id = self.main_panel.create_rectangle(
                    #     tmp[1], tmp[2], tmp[3], tmp[4], width=2,
                    #     outline=COLORS[(len(self.bbox_list) - 1) % len(COLORS)]
                    # )
                    # self.bbox_id_list.append(tmp_id)
                    # self.list_box.insert(END, '(%d, %d) -> (%d, %d)' % (tmp[0], tmp[1], tmp[2], tmp[3]))
                    # self.list_box.itemconfig(
                    #     len(self.bbox_id_list) - 1,
                    #     fg=COLORS[(len(self.bbox_id_list) - 1) % len(COLORS)]
                    # )

    def save_data(self):
        self.save_btn_text.set("Saving ...")
        try:
            category = int(self.category_entry.get())
        except Exception:
            showerror(self.window_label, "Category ID not specified")
            self.save_btn_text.set("Save")
            return -1
        width_origin = self.cur_img.width
        height_origin = self.cur_img.height
        for angle in [0, 90, 180, 270]:
            rotated = imutils.rotate_bound(self.cur_img_cv2, angle)
            name_no_ext = self.images_names[self.cur_img_id].split('.')[0] + "_" + str(angle) + "_degree"
            cv2.imwrite(self.data_dir + "/" + name_no_ext + ".jpg", rotated)
            (r_h, r_w) = rotated.shape[:2]
            boxes_list = []
            for box in self.boxes_list:
                boxes_list.append(self.get_coordinates(box, angle, width_origin, height_origin))
            self.save_single_data(self.data_dir + "/" + name_no_ext + ".txt", category, boxes_list, r_w, r_h)
            print('Data of image %s (rotated at %d) saved' % (self.images_names[self.cur_img_id], angle))
        self.save_btn_text.set("Save")

    @staticmethod
    def get_coordinates(box, angle, width_origin, height_origin):
        x_min_origin = box[0]
        x_max_origin = box[2]
        y_min_origin = box[1]
        y_max_origin = box[3]
        if angle == 90:
            x_min = height_origin - y_max_origin
            x_max = height_origin - y_min_origin
            y_min = x_min_origin
            y_max = x_max_origin
        elif angle == 180:
            x_min = width_origin - x_max_origin
            x_max = width_origin - x_min_origin
            y_min = height_origin - y_max_origin
            y_max = height_origin - y_min_origin
        elif angle == 270:
            x_min = y_min_origin
            x_max = y_max_origin
            y_min = width_origin - x_max_origin
            y_max = width_origin - x_min_origin
        else:
            x_min = x_min_origin
            x_max = x_max_origin
            y_min = y_min_origin
            y_max = y_max_origin
        return x_min, y_min, x_max, y_max

    def save_single_data(self, file_name, category, boxes_list, width, height):
        with open(file_name, 'w') as file:
            file.write('%d ' % category)
            for box in boxes_list:
                x_min = box[0]
                x_max = box[2]
                y_min = box[1]
                y_max = box[3]
                b = (float(x_min), float(x_max), float(y_min), float(y_max))
                file.write(' '.join(map(str, self.convert_to_yolo((width, height), b))) + ' ')

    @staticmethod
    def convert_to_yolo(size, box):
        dw = 1.0 / size[0]
        dh = 1.0 / size[1]
        x = (box[0] + box[1]) / 2.0
        y = (box[2] + box[3]) / 2.0
        w = box[1] - box[0]
        h = box[3] - box[2]
        x = x * dw
        w = w * dw
        y = y * dh
        h = h * dh
        # print("Convert %s %s to %f %f %f %f" % (size, box, x, y, w, h))
        return x, y, w, h

    def mouse_click(self, event):
        if self.state['click'] == 0:
            self.state['x'], self.state['y'] = event.x, event.y
        else:
            x1, x2 = min(self.state['x'], event.x), max(self.state['x'], event.x)
            y1, y2 = min(self.state['y'], event.y), max(self.state['y'], event.y)
            self.boxes_list.append((x1, y1, x2, y2))
            self.bbox_id_list.append(self.bbox_id)
            self.bbox_id = None
            self.list_box.insert(END, '(%d, %d) -> (%d, %d)' % (x1, y1, x2, y2))
            self.list_box.itemconfig(len(self.bbox_id_list) - 1, fg=COLORS[(len(self.bbox_id_list) - 1) % len(COLORS)])
        self.state['click'] = 1 - self.state['click']

    def mouse_move(self, event):
        if self.tk_img:
            if self.hl:
                self.main_panel.delete(self.hl)
            self.hl = self.main_panel.create_line(0, event.y, self.tk_img.width(), event.y, width=2)
            if self.vl:
                self.main_panel.delete(self.vl)
            self.vl = self.main_panel.create_line(event.x, 0, event.x, self.tk_img.height(), width=2)
        if 1 == self.state['click']:
            if self.bbox_id:
                self.main_panel.delete(self.bbox_id)
            self.bbox_id = self.main_panel.create_rectangle(self.state['x'], self.state['y'],
                                                            event.x, event.y,
                                                            width=2,
                                                            outline=COLORS[len(self.boxes_list) % len(COLORS)])

    def cancel_bbox(self):
        if 1 == self.state['click']:
            if self.bbox_id:
                self.main_panel.delete(self.bbox_id)
                self.bbox_id = None
                self.state['click'] = 0

    def del_bbox(self):
        sel = self.list_box.curselection()
        if len(sel) != 1:
            return
        idx = int(sel[0])
        self.main_panel.delete(self.bbox_id_list[idx])
        self.bbox_id_list.pop(idx)
        self.boxes_list.pop(idx)
        self.list_box.delete(idx)

    def clear_bbox(self):
        for idx in range(len(self.bbox_id_list)):
            self.main_panel.delete(self.bbox_id_list[idx])
        self.list_box.delete(0, len(self.boxes_list))
        self.bbox_id_list = []
        self.boxes_list = []

    def prev_image(self):
        if self.cur_img_id > 0:
            self.cur_img_id -= 1
            self.load_current_image()

    def next_image(self):
        if self.cur_img_id < self.total_imgs - 1:
            self.cur_img_id += 1
            self.load_current_image()

    def goto_image(self):
        idx = int(self.idx_entry.get())
        if 0 <= idx <= self.total_imgs:
            self.cur_img_id = idx
            self.load_current_image()

    def save_cache(self, key, value):
        """
        Saves a pair of key/value into permanent storage.
        Read more at https://docs.python.org/3/library/shelve.html
        :param key:
        :param value:
        :return:
        """
        storage = shelve.open(self.storage_file_name)
        storage[key] = value
        storage.close()

    def load_cache(self, key):
        """
        Loads a pair of key/value from permanent storage.
        Read more at https://docs.python.org/3/library/shelve.html
        :param key:
        :return:
        """
        storage = shelve.open(self.storage_file_name)
        value = None
        if key in storage:
            value = storage[key]
        storage.close()
        return value


if __name__ == '__main__':
    root = Tk()
    tool = LabelTool(root)
    root.resizable(width=True, height=True)
    root.mainloop()
