import glob
import os
import random
from time import time

import cv2

from utils import cv_image_resize, overlay_png_imgs

########################################################################################################################
# Custom Set Up.
########################################################################################################################

# Path to a folder contained background images. Those images are use as background for the positive one that describes
# target object. Remember, these images must be PNG as target image is also must be PNG, these must be identical in
# their structure in order to be blended into single one.
bg_imgs_path = "/home/tstone10/dev/yolo/bg"
# Path to a folder contained background images that will be use without any labels on them.
bg_imgs_no_label_path = "/home/tstone10/dev/yolo/bg_no_label"
# Path to a folder contained target images. These must be PNG with alpha channel.
# Structure of this folder must reflect categorization of network:
# - <pos_imgs_path>   folder with target images
#   - 0               folder with images related to category with id of 0
#   - 1               folder with images related to category with id of 1
#   ...                etc ...
pos_imgs_path = "/home/tstone10/dev/yolo/pos"
# Path to a destination folder, where results will be saved.
dst_path = "/home/tstone10/dev/yolo/dst"
# Percentage of images to be used for the test set.
percentage_test = 10

########################################################################################################################
# Project logic.
########################################################################################################################
# TODO: Refactor to classes
# print("Prepare images")
# for i in range(0, 19):
#     cmd_src = "/home/tstone10/dev/yolo/src/data/{}".format(i)
#     cmd_dst = "/home/tstone10/dev/yolo/pos/{}".format(i)
#     cmd = "/home/tstone10/dev/driver-assistant-c/src/main/python/image_transform.py -s {} -d {}".format(cmd_src,
#                                                                                                         cmd_dst)
#     cwd = os.path.join(os.getcwd(), cmd)
#     os.system('{} {}'.format('python3', cmd))
#     cmd = "/home/tstone10/dev/driver-assistant-c/src/main/python/detect_and_remove.py -d {}".format(cmd_dst)
#     os.system('{} {}'.format('python3', cmd))

print("Prepare data set")
img_ext_png = ".png"
img_ext_jpg = ".jpg"
txt_ext = ".txt"
color_format_rgb = "RGB"
color_format_gray = "GRAY"

if not os.path.exists(dst_path):
    print("Dst dir does not exist. Create %s." % dst_path)
    os.makedirs(dst_path)

bg_imgs_num = len(os.listdir(bg_imgs_path))
pos_imgs_num = 0
# Array of background image names.
bg_imgs = []
# Array of background image names to use with no labels on them.
bg_imgs_no_label = []
bg_img_no_label_idx = 0
# Number of categories.
cat_num = len(os.listdir(pos_imgs_path))
# Array of target image names. This is 2-d array where first element is category id and the second is a list of actual
# image names in category.
pos_imgs = [0] * cat_num

# Initialize essential values, such as counters, arrays, etc ... .
for cat_dir_name in os.listdir(pos_imgs_path):
    category = int(cat_dir_name)
    pos_imgs_in_cat = []
    pos_imgs_sub_dir_path = pos_imgs_path + "/" + cat_dir_name
    pos_imgs_num += len(os.listdir(pos_imgs_sub_dir_path))
    for pos_img_name in os.listdir(pos_imgs_sub_dir_path):
        pos_imgs_in_cat.append(pos_img_name)
    pos_imgs[category] = pos_imgs_in_cat
# Fill array of background image names.
for bg_file_name in os.listdir(bg_imgs_path):
    bg_imgs.append(bg_file_name)
for img in os.listdir(bg_imgs_no_label_path):
    bg_imgs_no_label.append(img)

print("%d pos. imgs, %d bg. imgs" % (pos_imgs_num, bg_imgs_num))


def convert_to_yolo(in_size, in_box):
    """

    :param in_size:
    :param in_box:
    :return:
    """
    dw = 1.0 / in_size[0]
    dh = 1.0 / in_size[1]
    x = (in_box[0] + in_box[1]) / 2.0
    y = (in_box[2] + in_box[3]) / 2.0
    w = in_box[1] - in_box[0]
    h = in_box[3] - in_box[2]
    x = x * dw
    w = w * dw
    y = y * dh
    h = h * dh
    # print("Convert %s %s to %f %f %f %f" % (size, box, x, y, w, h))
    return x, y, w, h


def save_text_data(in_file_name, in_category, in_boxes_list, in_width, in_height):
    """
    Writes data associated with image label and boundary box into a file that represents train image.
    :param in_file_name:
    :param in_category:
    :param in_boxes_list:
    :param in_width:
    :param in_height:
    :return:
    """
    with open(in_file_name, 'w') as file:
        # Write an empty file in case of no category specified. This is intentional technique.
        if in_category is None:
            file.write('')
        # Write label id and boundary box otherwise.
        else:
            file.write('%d ' % in_category)
            for box in in_boxes_list:
                x_min = box[0]
                x_max = box[2]
                y_min = box[1]
                y_max = box[3]
                b = (float(x_min), float(x_max), float(y_min), float(y_max))
                file.write(' '.join(map(str, convert_to_yolo((in_width, in_height), b))) + ' ')


def process_img(in_category, in_pos_img, in_i):
    """
    Process single train image.
    :param in_category:
    :param in_pos_img:
    :param in_i:
    :return:
    """
    global bg_imgs_path, bg_imgs, pos_imgs
    bg_img = cv2.imread(bg_imgs_path + "/" + bg_imgs.pop(random.randint(0, len(bg_imgs) - 1)), cv2.IMREAD_UNCHANGED)
    bg_img = cv2.cvtColor(bg_img, cv2.COLOR_GRAY2RGB)
    in_pos_img_cpy = in_pos_img.copy()
    bg_img_h, bg_img_w = bg_img.shape[:2]
    in_pos_img_h, in_pos_img_w = in_pos_img_cpy.shape[:2]

    # Sometimes this happen. Scale image to fit background one.
    is_scaled = False
    if bg_img_h <= in_pos_img_h or bg_img_w <= in_pos_img_w:
        in_pos_img_w_org = in_pos_img_w
        in_pos_img_h_org = in_pos_img_h
        bg_size_min = bg_img_w if bg_img_w <= bg_img_h else bg_img_h
        bg_size_min = int(bg_size_min / 2)
        in_pos_size_max = in_pos_img_w if in_pos_img_w >= in_pos_img_h else in_pos_img_h
        scale_f = in_pos_size_max / bg_size_min
        in_pos_img_cpy = cv_image_resize(in_pos_img_cpy, in_pos_img_w / scale_f, in_pos_img_h / scale_f)
        in_pos_img_h, in_pos_img_w = in_pos_img_cpy.shape[:2]
        is_scaled = True
        print("      scale down {}x{} to {}x{}".format(in_pos_img_w_org, in_pos_img_h_org, in_pos_img_w, in_pos_img_h))

    x = random.randint(0, bg_img_w - in_pos_img_w)
    y = random.randint(0, bg_img_h - in_pos_img_h)
    overlay_png_imgs(bg_img, in_pos_img_cpy, x, y)

    ms = str(time() * 1000.0).replace(".", "")
    path = dst_path + "/" + str(ms) + "_" + pos_imgs[in_category][in_i]
    path_txt = path.replace(img_ext_png, txt_ext)
    path_jpg = path.replace(img_ext_png, img_ext_jpg)
    bg_img = cv2.cvtColor(bg_img, cv2.COLOR_BGR2GRAY)
    # Draw rect to validate the rest is in correct place
    # color = (255, 0, 0)
    # start_point = (x, y)
    # end_point = (x + in_pos_img_w, y + in_pos_img_h)
    # bg_img = cv2.rectangle(bg_img, start_point, end_point, color, 2)
    cv2.imwrite(path_jpg, bg_img, [int(cv2.IMWRITE_JPEG_QUALITY), 100])

    boxes_list = [(x, y, x + in_pos_img_w, y + in_pos_img_h)]
    save_text_data(path_txt, in_category, boxes_list, bg_img_w, bg_img_h)
    if is_scaled:
        print("      scaled img {}".format(path_jpg))


def process_img_no_label():
    """
    Process single image with no label on it.
    :return:
    """
    global bg_imgs_no_label_path, bg_imgs_no_label, bg_img_no_label_idx
    if bg_img_no_label_idx >= len(bg_imgs_no_label):
        return False
    img_no_label = cv2.imread(bg_imgs_no_label_path + "/" + bg_imgs_no_label[bg_img_no_label_idx], cv2.IMREAD_UNCHANGED)

    ms = str(time() * 1000.0).replace(".", "")
    path = dst_path + "/" + str(ms) + "_" + bg_imgs_no_label[bg_img_no_label_idx]
    path_txt = path.replace(img_ext_png, txt_ext)
    path_jpg = path.replace(img_ext_png, img_ext_jpg)
    img_no_label = cv2.cvtColor(img_no_label, cv2.COLOR_BGR2GRAY)
    cv2.imwrite(path_jpg, img_no_label, [int(cv2.IMWRITE_JPEG_QUALITY), 90])

    save_text_data(path_txt, None, None, None, None)

    bg_img_no_label_idx += 1
    return True


# Iterate over categories of target images.
for cat_imgs in pos_imgs:
    category = pos_imgs.index(cat_imgs)
    print("Process category %s (%d imgs)" % (category, len(cat_imgs)))
    # In each category, iterate over concrete target images.
    for i in range(0, len(cat_imgs)):
        full_path = pos_imgs_path + "/" + str(category) + "/" + pos_imgs[category][i]
        pos_img = cv2.imread(full_path, cv2.IMREAD_UNCHANGED)
        process_img(category, pos_img, i)
        # For each target image use at least one with no label on it.
        # process_img_no_label()

print("Images and txt descriptions prepared.")

train_paths = []
for path_and_name in glob.iglob(os.path.join(dst_path, "*" + img_ext_jpg)):
    title, ext = os.path.splitext(os.path.basename(path_and_name))
    train_paths.append(title)

num_imgs = len(train_paths)
print("Initial data set size {}".format(num_imgs))
index_test = int(percentage_test * num_imgs / 100)
test_paths = [train_paths.pop(random.randrange(len(train_paths))) for _ in range(index_test)]

print("Total {}, in train {}, in test {}".format(num_imgs, len(train_paths), len(test_paths)))

# Create and/or truncate 'train.txt' and 'test.txt'.
file_train = open(dst_path + '/train.txt', 'w')
file_test = open(dst_path + '/test.txt', 'w')

for p in test_paths:
    file_test.write(dst_path + "/" + p + img_ext_jpg + "\n")

for p in train_paths:
    file_train.write(dst_path + "/" + p + img_ext_jpg + "\n")

print("train.txt and test.txt populated.\nData prepared.\nHave fun with DNN training ☺")
