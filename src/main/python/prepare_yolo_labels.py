import os
import sys
from os.path import dirname, abspath

import cv2
import numpy as np

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

file_ext = ".png"
src = "/home/yurii/Pictures/road_signs/stop/imgs"
labels_dir = src + "/labels"
if not os.path.exists(labels_dir):
    os.mkdir(labels_dir)


def convert_to_yolo(size, box):
    dw = 1. / size[0]
    dh = 1. / size[1]
    x = (box[0] + box[1]) / 2.0
    y = (box[2] + box[3]) / 2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x * dw
    w = w * dw
    y = y * dh
    h = h * dh
    print("Convert %s %s to %f %f %f %f" % (size, box, x, y, w, h))
    return x, y, w, h


for filename in os.listdir(src):
    if filename.endswith(file_ext):
        img = cv2.imread(os.path.join(src, filename), cv2.IMREAD_UNCHANGED)
        height = np.size(img, 0)
        width = np.size(img, 1)
        print("File %s [%dx%d]" % (filename, width, height))
        x_min = 0
        x_max = width
        y_min = 0
        y_max = height
        b = (float(x_min), float(x_max), float(y_min), float(y_max))

        image_name = os.path.split(filename)[-1].split('.')[0]
        label_name = image_name + '.txt'
        label_file_name = labels_dir + "/" + label_name
        with open(label_file_name, 'w') as f:
            f.write('%d ' % 0)
            f.write(' '.join(map(str, convert_to_yolo((width, height), b))) + ' ')
    else:
        print("File '%s' is not an image" % filename)
        continue

print("Labels are prepared")
