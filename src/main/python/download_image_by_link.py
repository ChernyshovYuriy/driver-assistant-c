import os
import urllib.request
from pathlib import Path
from time import time

import cv2
import requests

from utils import cv_image_resize

# Array of ids that describes category from ImageNet
ids = {
    "n04096066",
    "n04335209",
    "n04334599",
    "n06266633",
    "n03930313",
    "n03000134",
    "n04115256",
    "n03215508",
    "n13912260",
    # "n13104059",
    "n12513613",
    "n02897820",
    "n02681392",
    "n09287968",
    "n00523513",
    "n07881800",
    "n04451818",
    "n03309808",
    "n04105893",
    # "n02729837"
}

# Alternative list of urls
urls = "/home/tstone10/Downloads/urls2.txt"

# Destination directory to save images
dest_dir_name = "/home/tstone10/dev/yolo/bg"

dataset_base_url = "http://www.image-net.org/api/text/imagenet.synset.geturls?wnid="
print("Downloading images to  :", dest_dir_name)


def download_image(image_url):
    try:
        print("Download:", image_url)
        ms = time() * 1000.0
        file_path = (dest_dir_name + "/" + str(ms)).replace(".", "") + ".png"
        file_path_tmp = dest_dir_name + "/tmp.img"

        # Make the actual request, set the timeout for no data to 10 seconds and enable streaming responses so we
        # don't have to keep the large files in memory.
        request = requests.get(image_url, timeout=10, stream=True)
        # Open the output file and make sure we write in binary mode.
        with open(file_path_tmp, "wb") as fh:
            # Walk through the request response in chunks of 1024 * 1024 bytes, so 1MB.
            for chunk in request.iter_content(1024 * 1024):
                # Write the chunk to the file.
                fh.write(chunk)
                # Optionally we can check here if the download is taking too long.

        img = cv2.imread(file_path_tmp, cv2.IMREAD_GRAYSCALE)
        # Image is empty
        if img is None:
            os.remove(file_path_tmp)
            print("Remove an empty image (%s)" % file_path_tmp)
            return
        # channels_nun = len(img.shape)
        # Image is not PNG, most likely it is JPG.
        # if channels_nun != 3:
        #     os.remove(file_path)
        #     print("Remove non PNG image (%s)" % file_path)
        #     continue
        resized_image = cv_image_resize(img, 1000, 1000)
        cv2.imwrite(file_path, resized_image)
        # Image size is too small
        img_size = Path(file_path).stat().st_size
        if img_size <= 100000:
            os.remove(file_path)
            os.remove(file_path_tmp)
            print("Remove small image (%s)" % file_path)
            return
        os.remove(file_path_tmp)
        print("  file '", file_path, "'")
    except Exception as e:
        print("  ", str(e))


def store_raw_images(id):
    global dest_dir_name
    dataset_url = dataset_base_url + id
    print("Downloading images from:", dataset_url)
    images_url = urllib.request.urlopen(dataset_url).read().decode()

    if not os.path.exists(dest_dir_name):
        os.makedirs(dest_dir_name)

    for image_url in images_url.split('\n'):
        download_image(image_url)


# for i in ids:
#     print("Dataset id:", i)
#     store_raw_images(i)

for i in open(urls).read().strip().split("\n"):
    download_image(i)

print("Images downloaded")
