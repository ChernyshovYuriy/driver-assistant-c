from pathlib import Path

path = Path("https://cars.usnews.com/dims4/USNEWS/9768fad/2147483647/resize/640x420%3E/format/jpeg/quality/85/?url=https%3A%2F%2Fcars.usnews.com%2Fstatic%2Fimages%2Farticle%2F202001%2F128365%2F1_title_2020_kia_sorento_640x420.jpg")
sep = "?"
suffix = path.suffix.split(sep, 1)[0].lower()

print("Name {}".format(suffix))