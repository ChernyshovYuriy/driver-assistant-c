import os

from PIL import Image, ImageFile

# Path of the directory with JPG images
src = "/home/tstone10/dev/yolo/bg_non_labeled/01"

# Use this to avoid IOError: image file is truncated (N bytes not processed)
ImageFile.LOAD_TRUNCATED_IMAGES = True

counter = 0
for file_name in os.listdir(src):
    if file_name.endswith(".jpg") or file_name.endswith(".JPG") or file_name.endswith(".jpeg"):
        img = Image.open(os.path.join(src, file_name)).convert("RGBA")
        p = os.path.sep.join((src, file_name))
        os.remove(p)

        file_name_new = os.path.splitext(file_name)[0] + ".png"
        file_name_full = os.path.join(src, file_name_new)
        img.save(
            file_name_full,
            "PNG", compress_level=0, optimize=False
        )
        print("Image", file_name, "converted to", file_name_new)
        counter += 1
    else:
        print("File is not JPEG image:", file_name)
        continue

print("{} images converted".format(counter))
