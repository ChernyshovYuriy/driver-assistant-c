import argparse
import os

import cv2
from imutils import paths

########################################################################################################################
# Command line arguments.
########################################################################################################################

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dst", required=False,
                help="Destination directory to processed images on.")
args = vars(ap.parse_args())

dst = args["dst"]
dst = "/home/tstone10/dev/yolo/bg/"
print("Dst: {}".format(dst))


def dhash(img, hash_size=8):
    # convert the image to grayscale and resize the grayscale image,
    # adding a single column (width) so we can compute the horizontal
    # gradient
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    resized = cv2.resize(gray, (hash_size + 1, hash_size))

    # compute the (relative) horizontal gradient between adjacent
    # column pixels
    diff = resized[:, 1:] > resized[:, :-1]

    # convert the difference image to a hash and return it
    return sum([2 ** i for (i, v) in enumerate(diff.flatten()) if v])


# grab the paths to all images in our input dataset directory and
# then initialize our hashes dictionary
print("Computing image hashes...")
imagePaths = list(paths.list_images(dst))
hashes = {}

# loop over our image paths
for imagePath in imagePaths:
    # load the input image and compute the hash
    image = cv2.imread(imagePath)
    h = dhash(image)

    # grab all image paths with that hash, add the current image
    # path to it, and store the list back in the hashes dictionary
    p = hashes.get(h, [])
    p.append(imagePath)
    hashes[h] = p

print("Clear duplicates...")
count = 0
# loop over the image hashes
for (h, hashedPaths) in hashes.items():
    # check to see if there is more than one image with the same hash
    if len(hashedPaths) > 1:
        # loop over all image paths with the same hash *except*
        # for the first image in the list (since we want to keep
        # one, and only one, of the duplicate images)
        print("   org {}".format(hashedPaths[0]))
        for p in hashedPaths[1:]:
            os.remove(p)
            print("      dup {}".format(p))
            count += 1

print("{} duplicated images".format(count))
