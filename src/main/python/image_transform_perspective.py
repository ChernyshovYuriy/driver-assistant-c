from random import randint

import numpy
from PIL import Image


def find_coefficients(pa, pb):
    matrix = []
    for p1, p2 in zip(pa, pb):
        matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0] * p1[0], -p2[0] * p1[1]])
        matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1] * p1[0], -p2[1] * p1[1]])

    a = numpy.matrix(matrix, dtype=numpy.float)
    b = numpy.array(pb).reshape(8)

    res = numpy.dot(numpy.linalg.inv(a.T * a) * a.T, b)
    return numpy.array(res).reshape(8)


img = Image.open("/home/tstone10/dev/yolo/pos/0/test/01.png")
img_width, img_height = img.size

width = 300
height = 300

img = img.resize((width, height), Image.ANTIALIAS)

num_of_coefficient_variants = 3
coefficient_id = randint(0, num_of_coefficient_variants)
shift = randint(5, 100)

if coefficient_id == 0:
    """
    Transform right side in perspective
    """
    coefficients = find_coefficients(
        [(0, 0), (width, 0), (width, height), (0, height)],
        [(0, 0), (width + shift, -shift), (width + shift, height + shift), (0, height)])
elif coefficient_id == 1:
    """
    Transform left side in perspective
    """
    coefficients = find_coefficients(
        [(0, 0), (width, 0), (width, height), (0, height)],
        [(-shift, -shift), (width, 0), (width, height), (-shift, height + shift)])
elif coefficient_id == 2:
    """
    Transform top side in perspective
    """
    coefficients = find_coefficients(
        [(0, 0), (width, 0), (width, height), (0, height)],
        [(-shift, -shift), (width + shift * 2, -shift), (width, height), (0, height)])
else:
    """
    No transform
    """
    coefficients = find_coefficients(
        [(0, 0), (width, 0), (width, height), (0, height)],
        [(0, 0), (width, 0), (width, height), (0, height)])

img = img.transform((width, height), Image.PERSPECTIVE, coefficients, Image.BICUBIC)
img.show()
