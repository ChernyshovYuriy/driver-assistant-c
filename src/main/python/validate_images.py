import os

import cv2

src = "/home/yurii/Pictures/bg"

print("Validate images at '", src, "'")


img_current_count = 0
img_total = len(os.listdir(src))
for filename in os.listdir(src):
    if filename.endswith(".png"):
        # print(os.path.join(directory, filename))
        try:
            img = cv2.imread(os.path.join(src, filename))
            img_len = len(img)
            print("\033[0;31;47m Bright Red")
            print("Image", len(img), "processed")
        except Exception as e:
            print("Exception:", filename, str(e))
            os.remove(os.path.join(src, filename))
    else:
        print("File is not PNG image:", filename)

print("Images validated")