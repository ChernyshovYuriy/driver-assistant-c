import glob
import os

# Directory where the data will reside, relative to 'darknet.exe'
path_data = '/home/yurii/Pictures/road_signs/stop/yolo-imgs'

# Percentage of images to be used for the test set
percentage_test = 10
img_ext = ".jpg"

# Create and/or truncate train.txt and test.txt
file_train = open(path_data + '/train.txt', 'w')
file_test = open(path_data + '/test.txt', 'w')

# Populate train.txt and test.txt
counter = 1
index_test = round(100 / percentage_test)
for pathAndFilename in glob.iglob(os.path.join(path_data, "*" + img_ext)):
    title, ext = os.path.splitext(os.path.basename(pathAndFilename))
    if counter == index_test:
        counter = 1
        file_test.write(path_data + "/" + title + img_ext + "\n")
    else:
        file_train.write(path_data + "/" + title + img_ext + "\n")
        counter = counter + 1
