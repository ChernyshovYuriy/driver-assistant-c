import PIL
import cv2


def pil_image_resize(image, resize_to):
    max_s = image.width
    if image.height > image.width:
        max_s = image.height
    ratio = float(resize_to / max_s)
    return image.resize((int(image.width * ratio), int(image.height * ratio)), resample=PIL.Image.BICUBIC)


def cv_image_resize(image, width, height, inter=cv2.INTER_AREA, aspect_ratio=True):
    image_h, image_w = image.shape[:2]
    if image_h <= height and image_w <= width:
        return image
    if aspect_ratio is True:
        h_ratio = height / float(image_h)
        w_ratio = width / float(image_w)
        ratio = min(h_ratio, w_ratio)
        return cv2.resize(image, dsize=(0, 0), fx=ratio, fy=ratio, interpolation=inter)
    else:
        dim = (width, height)
        return cv2.resize(image, dim, interpolation=inter)


def blend_imgs(front_img, back_img, anchor_y, anchor_x):
    foreground, background = front_img.copy(), back_img.copy()
    # Check if the foreground is inbound with the new coordinates and raise an error if out of bounds
    background_height = background.shape[1]
    background_width = background.shape[1]
    foreground_height = foreground.shape[0]
    foreground_width = foreground.shape[1]
    if foreground_height + anchor_y > background_height or foreground_width + anchor_x > background_width:
        raise ValueError("The foreground image exceeds the background boundaries at this location")

    alpha = 1

    # do composite at specified location
    start_y = anchor_y
    start_x = anchor_x
    end_y = anchor_y + foreground_height
    end_x = anchor_x + foreground_width
    blended_portion = cv2.addWeighted(foreground,
                                      alpha,
                                      background[start_y:end_y, start_x:end_x, :],
                                      0,
                                      0,
                                      background)
    background[start_y:end_y, start_x:end_x, :] = blended_portion
    return background


def overlay_png_imgs(bg_img, fg_img, x, y):
    """
    Overlay one png image with alpha channel over another.
    Foreground image must be the same size with background one or smaller, but not bigger.
    :param bg_img:
    :param fg_img:
    :param x:
    :param y:
    :return:
    """
    y1, y2 = y, y + fg_img.shape[0]
    x1, x2 = x, x + fg_img.shape[1]
    alpha_fg = fg_img[:, :, 3] / 255.0
    alpha_bg = 1.0 - alpha_fg
    for c in range(0, 3):
        bg_img[y1:y2, x1:x2, c] = (alpha_fg * fg_img[:, :, c] + alpha_bg * bg_img[y1:y2, x1:x2, c])
