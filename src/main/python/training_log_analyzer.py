import matplotlib.pyplot as plt

src = "/home/vz3k6l/Documents/DarknetTraining"

count = 0
min = 100000
max = -100000
weights = []
avgs = []
with open(src) as infile:
    for line in infile:
        if " avg, " not in line:
            continue
        items = line.split(", ")
        count += 1
        # print("Line:%s" % items)
        for item in items:
            if "avg" not in item:
                continue
            item = float(item.replace(" avg", ""))
            if item > .1:
                continue
            weight = int(items[0].split(":")[0])
            avgs.append(item)
            weights.append(weight)
            print("Avg:%.3f at %s, item:%s" % (item, weight, items))
print("Num of lines:%d" % count)

# X, Y
plt.plot(weights, avgs)
plt.xlabel('Weights')
plt.ylabel('Avgs')
plt.show()