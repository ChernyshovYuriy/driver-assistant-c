import os

src = "/home/yurii/Pictures/road_signs/stop/yolo-imgs/labels"
ext = ".txt"

for file_name in os.listdir(src):
    if file_name.endswith(ext):
        file = open(os.path.join(src, file_name), "r")
        text = file.read()
        text = text.replace('1', '0', 1).strip()

        file = open(os.path.join(src, file_name), "w")
        file.write(text)
        file.close()

        print("File %s\t:\t%s" % (file_name, text))
    else:
        print("File is not text:", file_name)
        continue

print("Files converted")