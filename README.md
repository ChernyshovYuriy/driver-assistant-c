# README #

This project uses [OpenCV](http://opencv.org/) library and Deep Neural Network (DefaultDNN) to detect and recognize road signs and pedestrians. It uses C++ as base language.

[Darknet](https://pjreddie.com/darknet/) is a framework used to train DefaultDNN model.

Current master branch is in develop and has a lot of modifications related to DefaultDNN use.

For stable version, which utilizes OpenCV version 3 and HAAR Cascade Classifier refer to **"legacy_opencv3"** branch.

### What is this repository for? ###

This repository is for everyone who is interesting in computer vision and its appliance to vehicle-related problems.

Repository is non-commercial open-sourced and free to use and contribute.

### License ###

This project is covered by the [Apache License](https://bitbucket.org/ChernyshovYuriy/driver-assistant-c/wiki/License).

### How do I get set up? ###

* Repository is ready to use if all dependencies are set up.
* This project is designed to run on environment which supports calculation on GPU, such as [NVIDIA Jetson Nano platform](https://developer.nvidia.com/embedded/jetson-nano-developer-kit). Cmake is handle platform dependent code during make time.
* It also can run on Ubuntu 16.04 / 18.04 LTS without GPU support (purely on CPU), however, result of detection is unstable.
* **Dependencies**: C++ 14, OpenCV 4.1.1 (or newer) and OpenCV contrib 4.1.1 (or newer), [cmake](https://cmake.org/). For Jetson Nano dependencies, please refer to their [documentation](https://developer.nvidia.com/embedded/jetson-nano-developer-kit).
* If video mode selected to process video file, make sure that all depended libraries are installed, such as gstreamer, etc ...

### How do I run it? ###

Create directory to clone project to and clone source code:
```
~$ mkdir EyeOnTheRoadCpp
~$ cd EyeOnTheRoadCpp/
~/EyeOnTheRoadCpp$ git clone git@bitbucket.org:ChernyshovYuriy/driver-assistant-c.git ~/EyeOnTheRoadCpp/
```
Create directory to build project to and build project:
```
~/EyeOnTheRoadCpp$ mkdir build
~/EyeOnTheRoadCpp$ cd build
```
Run cmake to generate project files into 'build' directory:
```
~/EyeOnTheRoadCpp/build$ cmake -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles" ../
```
Run cmake to generate binary executable:
```
~/EyeOnTheRoadCpp/build$ cmake --build . --target EyeOnTheRoadCpp -- -j 4
```
Run project:
```
cd ../
~/EyeOnTheRoadCpp$ build/EyeOnTheRoadCpp
```

[![Nvidia Jetson Nano](http://img.youtube.com/vi/UGzXEio46pQ/0.jpg)](http://www.youtube.com/watch?v=UGzXEio46pQ)

### Who do I talk to? ###

Project owner and admin - Yurii Chernyshov

E-mail : chernyshov.yuriy@gmail.com

LinkedIn: https://www.linkedin.com/in/yurii-chernyshov/
